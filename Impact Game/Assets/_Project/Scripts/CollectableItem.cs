﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableItem : MonoBehaviour
{
    public void OnCollect()
    {
        SFXManager.Instance.PlayCollectSound();
        GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.starCollect);
    }
}
