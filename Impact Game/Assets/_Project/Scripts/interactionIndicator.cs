﻿using UnityEngine;

public class interactionIndicator : MonoBehaviour
{
    [SerializeField] public float disableAfterTime = -1;
    [SerializeField] private GameEvent[] interactionEvents;

    private void OnEnable()
    {
        for (int i = 0; i < interactionEvents.Length; i++)
        {
            interactionEvents[i].RegisterFunction(OnActionDone);
        }
        
        if (disableAfterTime >= 0)
        {
            CancelInvoke(nameof(OnActionDone));
            Invoke(nameof(OnActionDone), disableAfterTime);
        }
    }

    private void OnDisable()
    {
        for (int i = 0; i < interactionEvents.Length; i++)
        {
            interactionEvents[i].RemoveFunction(OnActionDone);
        }
    }

    private void OnActionDone()
    {
        gameObject.SetActive(false);
        OnDisable();
    }
}