﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(DragableUi))]
public class DragableFlyback : MonoBehaviour
{
    [SerializeField] private DragableUi dragableUi;
    [SerializeField] private float touchRadius = 0.5f;
    [SerializeField] private LayerMask touchingLayer;
    [SerializeField] private float waitBeforeFlyBack = 1;

    [Space]
    [SerializeField] private BoolVariable isDraggingSomething;

    public delegate void DragOnSomething(GameObject other);
    public event DragOnSomething onDragOnSomething;
    public event DragOnSomething onHoveredOnSomething;
    public event DragableUi.OnDragChange onDragEnd;
    public event DragableUi.OnDragChange onDragStart;
    public event DragableUi.OnDragChange onDragging;

    public Vector3 initialPosition;
    public RectTransform rect;
    private Collider2D[] collidingItems = new Collider2D[1];

    private void OnEnable()
    {
        rect = gameObject.GetComponent<RectTransform>();
        initialPosition = rect.anchoredPosition;

        onDragEnd = () => { StartCoroutine(DragEnd()); };
        onDragStart = () => { isDraggingSomething.SetTrue(); };
        onDragging = Dragging;
        dragableUi.onDragEnd += onDragEnd;
        dragableUi.onDragStart += onDragStart;
        dragableUi.onDragging += onDragging;
    }

    private void OnDisable()
    {
        dragableUi.onDragEnd -= onDragEnd;
        dragableUi.onDragEnd -= onDragStart;
        dragableUi.onDragging -= onDragging;
    }

    private void Dragging()
    {
        collidingItems[0] = null;
        Physics2D.OverlapCircleNonAlloc(transform.position, touchRadius, collidingItems, touchingLayer);

        if (collidingItems.Length > 0)
        {
            if (collidingItems[0] != null)
            {
                onHoveredOnSomething.Invoke(collidingItems[0].gameObject);
                return;
            }
        }
        onHoveredOnSomething.Invoke(null);
    }

    IEnumerator DragEnd()
    {
        collidingItems[0] = null;
        Physics2D.OverlapCircleNonAlloc(transform.position, touchRadius, collidingItems, touchingLayer);

        if (collidingItems.Length > 0)
        {
            if (collidingItems[0] != null)
            {
                onDragOnSomething.Invoke(collidingItems[0].gameObject);
                yield return new WaitForSeconds(waitBeforeFlyBack);
            }
        }

        rect.DOAnchorPos(initialPosition, 1);
        isDraggingSomething.SetFalse();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, touchRadius);
    }
}