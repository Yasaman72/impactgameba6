﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CheckTouchingObjects : MonoBehaviour
{
    [SerializeField] private LayerMask _touchingLayer;
    [SerializeField] private float _touchRadius;
    [SerializeField] private InventoryItem inventoryItem;

    private GameObject otherObj;

    private Collider2D[] collidingItems = new Collider2D[1];

    public bool CheckTouchWithLayer(Vector3 initialPosition, InteractableItem interactableItem)
    {
        Physics2D.OverlapCircleNonAlloc(transform.position, _touchRadius, collidingItems, _touchingLayer);

        if (collidingItems.Length > 0)
        {
            if (collidingItems[0] != null)
            {
                otherObj = collidingItems[0].gameObject;

                SlotManager slotManager = otherObj.GetComponent<SlotManager>();
                if (slotManager.itemInSlot == null)
                {
                    slotManager.HoldNewItem(interactableItem, inventoryItem);
                    MoveBack(initialPosition, 0);
                    return true;
                }
            }
        }

        MoveBack(initialPosition, 0.5f);
        return false;
    }

    private void MoveBack(Vector3 pos, float time)
    {
        transform.DOLocalMove(pos, time);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, _touchRadius);
    }
}