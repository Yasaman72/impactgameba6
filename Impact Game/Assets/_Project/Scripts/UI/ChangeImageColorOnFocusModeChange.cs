﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeImageColorOnFocusModeChange : MonoBehaviour
{
    [Header("writes")]
    [SerializeField] private BoolVariable isInFocusMode;
    [SerializeField] private Image image;
    [SerializeField] private DragableUi dragableUi;
    private Color initialIconColor;

    private void OnEnable()
    {
        initialIconColor = image.color;
        
        isInFocusMode.AddListener(OnFocusModeChanged);
    }

    private void OnDisable()
    {
        isInFocusMode.RemoveListener(OnFocusModeChanged);
    }
    
    private void OnFocusModeChanged()
    {
        if (isInFocusMode.RuntimeValue)
        {
            image.color = new Color(initialIconColor.r, initialIconColor.g, initialIconColor.b, 0.5f);
            if (dragableUi != null) dragableUi.canDrag = false;
        }
        else
        {
            image.color = Color.white;
            if (dragableUi != null) dragableUi.canDrag = true;
        }
    }
}
