﻿using Sirenix.OdinInspector;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

public class SlotManager : MonoBehaviour
{
    [ReadOnly] public GameObject itemInSlot;

    [Space]
    [SerializeField] private BoolVariable isDraggingSomething;

    [Space]
    [SerializeField] private Collider2D slotCollider;
    [SerializeField] private GameObject slotIndicator;
    [SerializeField] private int itemSortingOrder;


    private Transform smokeEffect;
    private void Start()
    {
        OnDraggingInventoryItem();
    }

    private void OnEnable()
    {
        isDraggingSomething.AddListener(OnDraggingInventoryItem);
    }

    private void OnDisable()
    {
        isDraggingSomething.RemoveListener(OnDraggingInventoryItem);
    }

    private void OnDraggingInventoryItem()
    {
        if (itemInSlot != null)
        {
            slotCollider.enabled = false;
            slotIndicator.SetActive(false);
            return;
        }

        bool isDragging = isDraggingSomething.RuntimeValue;

        slotCollider.enabled = isDragging;
        slotIndicator.SetActive(isDragging);
    }

    public void HoldNewItem(InteractableItem interactableItem, InventoryItem InventoryItem)
    {
        GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.Smoke);

        itemInSlot = Instantiate(interactableItem.toyObject);

        itemInSlot.transform.SetParent(gameObject.transform);
        itemInSlot.transform.localPosition = Vector3.zero;
        itemInSlot.transform.localRotation = quaternion.identity;
        itemInSlot.transform.localScale = Vector3.one * interactableItem.placedScale;

        // itemInSlot.GetComponent<GeneralInteractaboleManager>().currentSlot = this;
        itemInSlot.GetComponent<GeneralInteractaboleManager>().InventoryItem = InventoryItem;

        itemInSlot.AddComponent<SortingGroup>();
        itemInSlot.GetComponent<SortingGroup>().sortingOrder = itemSortingOrder;
    }

    public void RemoveObject()
    {

        itemInSlot.GetComponent<GeneralInteractaboleManager>().InventoryItem.Enable();
        Destroy(itemInSlot);
        itemInSlot = null;
    }
}