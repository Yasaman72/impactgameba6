﻿using DG.Tweening;
using UnityEngine;

public class SparklesMove : MonoBehaviour
{
    [SerializeField] private float startWaitTime = 1;
    [SerializeField] private float moveDuration = 3;

    private Transform goalPosition;

    private void OnEnable()
    {
        goalPosition = BackgroundAppearWithSparkles.Instance.currentBackgroundTransform;
        if (goalPosition == null) return;
        Invoke(nameof(Move), startWaitTime);
    }

    private void Move()
    {
        transform.DOMove(goalPosition.position, moveDuration);
    }
}