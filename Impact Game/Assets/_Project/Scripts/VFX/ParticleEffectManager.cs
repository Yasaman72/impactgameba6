﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEffectManager : MonoBehaviour
{
    [SerializeField] private float destroyTimer = 5f;
    [SerializeField] private bool disableOnly;
    [SerializeField] private bool disableOnStart;
    [SerializeField] private ParticleSystem particles;

    private void Start()
    {
        if (disableOnStart) gameObject.SetActive(false);

        if (disableOnly)
        {
            CancelInvoke(nameof(Disable));
            Invoke(nameof(Disable), destroyTimer);
        }
        else
        {
            Destroy(gameObject, destroyTimer);
        }
    }

    private void OnEnable()
    {
        if (particles != null) particles.Play();
    }

    private void Disable()
    {
        gameObject.SetActive(false);
    }
}