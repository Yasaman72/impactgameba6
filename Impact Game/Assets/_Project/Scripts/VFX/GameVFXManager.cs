﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameVFXManager : MonoBehaviour
{
    public static GameVFXManager instance;
    
    public Transform zigzagLinesParticle;
    public Transform curlyLinesParticle;
    public Transform positiveParticle;
    public Transform smallPositiveParticle;
    public Transform smokeParticle;
    public Transform smallSmokeParticle;
    public Transform waterSplashParticle;
    public Transform starCollect;

    private Transform vfxEffect;
    private void Awake()
    {
        instance = this;
    }

    public enum ParticleType
    {
        Zigzag,
        CurlyLine,
        Positive,
        SmallPositive,
        Smoke,
        SmallSmoke,
        WaterSplash,
        starCollect
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    public GameObject SpawnParticleEffect(Transform spawnPoint,ParticleType type)
    {
        switch (type)
        {
            case ParticleType.Zigzag:
                vfxEffect = Instantiate(zigzagLinesParticle, spawnPoint);
                break;
            case ParticleType.CurlyLine:
                vfxEffect = Instantiate(curlyLinesParticle, spawnPoint);
                break;
            case ParticleType.Positive:
                vfxEffect = Instantiate(positiveParticle, spawnPoint);
                break;
            case ParticleType.SmallPositive:
                vfxEffect = Instantiate(smallPositiveParticle, spawnPoint);
                break;
            case ParticleType.Smoke:
                vfxEffect = Instantiate(smokeParticle, spawnPoint);
                break;
            case ParticleType.SmallSmoke:
                vfxEffect = Instantiate(smallSmokeParticle, spawnPoint);
                break;
            case ParticleType.WaterSplash:
                vfxEffect = Instantiate(waterSplashParticle, spawnPoint);
                break;
            case ParticleType.starCollect:
                vfxEffect = Instantiate(starCollect, spawnPoint);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
        
        vfxEffect.localPosition = Vector3.zero;
        vfxEffect.parent = null;
        vfxEffect.localScale = Vector3.one;
        return vfxEffect.gameObject;
    }
    
}
