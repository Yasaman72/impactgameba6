﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualEffectManager : MonoBehaviour
{
    public SpriteRenderer[] sprites;
    public float fadeSpeed;
    public bool doFade = true;


    private readonly Color zeroOpacityColor = new Color(1, 1, 1, 0);
    private readonly Color fullOpacityColor = new Color(1, 1, 1, 1);

    // Start is called before the first frame update
    private void OnEnable()
    {
        if(!doFade) return;
        
        for (int i = 0; i < sprites.Length; i++)
        {
            sprites[i].color = zeroOpacityColor;
        }
        StartCoroutine(FadeIn());
    }

    public void StartFadeOut()
    {
        if(!doFade) return;
        StopAllCoroutines();
        StartCoroutine(FadeOut());
    }
    
    public void FlipSprite()
    {
        for (int i = 0; i < sprites.Length; i++)
        {
            sprites[i].flipX = !sprites[i].flipX;
        }
    }

    public bool IsFlipDirectionRight()
    {
        return sprites[0].flipX;
    }

    private IEnumerator FadeIn()
    {
        while (sprites[sprites.Length - 1].color.a < 1)
        {
            for (int i = 0; i < sprites.Length; i++)
            {
                sprites[i].color = Color.Lerp(sprites[i].color, fullOpacityColor, Time.deltaTime * fadeSpeed);
            }
            yield return null;
        }

        StopAllCoroutines();
    }

    private IEnumerator FadeOut()
    {
        while (sprites[sprites.Length - 1].color.a > 0)
        {
            for (int i = 0; i < sprites.Length; i++)
            {
                sprites[i].color = Color.Lerp(sprites[i].color, zeroOpacityColor, Time.deltaTime * fadeSpeed);
            }
            
            yield return null;
        }

        StopAllCoroutines();
    }
}