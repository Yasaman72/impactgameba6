﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Interactible Items Manager", menuName = "Interactible/Interactible Items Manager",
                    order = 1)]
public class InteractableItemsManager : ScriptableObject
{
    public List<InteractableItem> collectedItems = new List<InteractableItem>();
    public GameEvent addedNewItemToInventory;

    public void AddToCollectedItems(InteractableItem item)
    {
        if (ContainTheItem(item)) return;

        item.isNew = true;
        collectedItems.Add(item);
        addedNewItemToInventory.Raise();
    }

    public bool ContainTheItem(InteractableItem item)
    {
        if (collectedItems.Contains(item)) return true;
        return false;
    }

    public int CollectedItemsCount()
    {
        return collectedItems.Count;
    }

    public void RemoveAllItems()
    {
        collectedItems.Clear();
    }
}