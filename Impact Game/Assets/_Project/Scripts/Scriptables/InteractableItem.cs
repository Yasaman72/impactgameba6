﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEditor;
using UnityEngine;


[CreateAssetMenu(fileName = "InteractableItem", menuName = "Interactible/Interactable Item", order = 1)]
public class InteractableItem : ScriptableObject
{
   public GameObject toyObject;
   public float placedScale;
   public float minRequiredCollectiveNeonStateTime;
   public Sprite itemInventoryIcon;
   public bool isEnableInInventory = true;
   public bool isNew = true;
   [ReadOnly] public bool isOnGround;
   
   
   private void OnEnable()
   {
      isEnableInInventory = true;
      isOnGround = false;
   }

}
