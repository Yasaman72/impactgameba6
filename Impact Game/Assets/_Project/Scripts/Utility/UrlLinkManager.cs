﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UrlLinkManager : MonoBehaviour
{
    [TextArea] public string m_urlLink;

    public void OpenLink()
    {
        Application.OpenURL(m_urlLink);
    }
}
