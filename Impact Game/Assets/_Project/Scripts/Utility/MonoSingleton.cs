﻿using UnityEngine;


public class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.Log(typeof(T).ToString() + " is NULL");

                _instance = FindObjectOfType<T>();
            }

            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this as T;
        AwakeInit();
    }

    public virtual void AwakeInit()
    {
    }
}