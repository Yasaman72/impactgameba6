﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UnloadLevel : MonoBehaviour
{
    [SerializeField] private SceneReference m_sceneToUnload;
    [SerializeField] private bool m_invoke;
    [SerializeField] private float m_unloadAfterTime;

    private void Start()
    {
        if (!m_invoke) return;
        Invoke(nameof(Unload), m_unloadAfterTime);
    }
    public void Unload()
    {
        SceneManager.UnloadSceneAsync(m_sceneToUnload);
    }
}
