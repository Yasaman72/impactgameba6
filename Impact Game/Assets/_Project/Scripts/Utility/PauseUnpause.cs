﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.SceneManagement;

public class PauseUnpause : MonoBehaviour
{
    [SerializeField] private BoolVariable isGamePaused;
    [SerializeField] private GameEvent pauseGame;
    [SerializeField] private GameEvent unpauseGame;

    private void OnEnable()
    {
        pauseGame.RegisterFunction(PauseGame);
        unpauseGame.RegisterFunction(UnpauseGame);
    }

    private void OnDisable()
    {
        pauseGame.RemoveFunction(PauseGame);
        unpauseGame.RemoveFunction(UnpauseGame);
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        isGamePaused.SetTrue();
    }

    public void UnpauseGame()
    {
        Time.timeScale = 1;
        isGamePaused.SetFalse();
    }
}
