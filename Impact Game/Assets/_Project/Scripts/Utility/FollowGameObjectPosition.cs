﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FollowGameObjectPosition : MonoBehaviour
{
    [SerializeField] private Transform m_target;
    [Space]
    [SerializeField] private bool m_follow;
    [SerializeField] private bool m_lookAt;


    void Update()
    {
        if(m_target == null || !m_target.gameObject.activeInHierarchy) return;

        if (m_follow)
        {
            if (m_target.position != gameObject.transform.position)
            {
                gameObject.transform.position = m_target.position;
            }
        }
        else if(m_lookAt)
        {
            gameObject.transform.LookAt(m_target);
        }
    }
}
