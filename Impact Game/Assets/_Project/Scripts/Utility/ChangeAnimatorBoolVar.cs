﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ChangeAnimatorBoolVar : MonoBehaviour
{
    [SerializeField] private BoolVariable m_event;
    [SerializeField] private Animator m_animator;
    [SerializeField] private string m_parameter;

    void OnEnable()
    {
        m_animator.SetBool(m_parameter, m_event.RuntimeValue);
        m_event.AddListener(ChangeParameter);
    }

    private void OnDisable()
    {
        m_event.RemoveListener(ChangeParameter);
    }

    void ChangeParameter()
    {
        m_animator.SetBool(m_parameter, m_event.RuntimeValue);
    }
}
