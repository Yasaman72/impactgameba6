﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorRandomStateSelection : MonoBehaviour
{
    [SerializeField] private Animator m_animator;
    [SerializeField] private animationStateData[] m_statesData;

    [System.Serializable]
    struct animationStateData
    {
        public string m_triggerParameterName;
        public string m_intParameterName;
        public int m_statesCount;
    }

    public void GoToState(string parameterName)
    {
        foreach (var stateData in m_statesData)
        {
            if (stateData.m_triggerParameterName == parameterName)
            {
                m_animator.SetTrigger(stateData.m_triggerParameterName);
                m_animator.SetInteger(stateData.m_intParameterName, Random.Range(0, stateData.m_statesCount));
            }
        }
    }
}
