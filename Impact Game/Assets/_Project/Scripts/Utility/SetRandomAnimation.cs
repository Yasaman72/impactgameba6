﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SetRandomAnimation : MonoBehaviour
{
    [Space]
    [SerializeField] private Animator animator;
    [SerializeField] private string animationParameter = "Index";
    [SerializeField] private int slotAnimationsCount;


    private void OnEnable()
    {
        animator.SetInteger(animationParameter, Random.Range(0, slotAnimationsCount));
    }
}
