﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderClick : MonoBehaviour
{
    [SerializeField] private bool m_debug;
    [SerializeField] private BoolVariable m_triggerDisabler;
    [SerializeField] private LayerMask m_layerMask;
    [SerializeField] private UnityEvent m_onClick;
    [SerializeField] private bool m_triggerOnce;
    [SerializeField] private BoolVariable m_triggered;

    //void OnMouseDown()
    //{
    //    m_onClick.Invoke();
    //}

    public void Update()
    {
        if(m_triggerDisabler != null && m_triggerDisabler.RuntimeValue) return;
        if (m_triggerOnce && m_triggered.RuntimeValue) return;

        if (Input.GetMouseButtonDown(0))
        {
            if (TryGetTouchPosition(out Vector2 touchPosition))
            {
                Ray cameraRay = Camera.main.ScreenPointToRay(new Vector3(touchPosition.x, touchPosition.y, 0));
                RaycastHit camHit;
                Debug.DrawRay(cameraRay.origin, cameraRay.direction * 20, Color.magenta);

                if (Physics.Raycast(cameraRay, out camHit, Mathf.Infinity, m_layerMask))
                {
                    if (m_debug)
                        Debug.Log("hit: " + camHit.collider.gameObject.name + "| from: " + gameObject.name,
                            camHit.collider.gameObject);
                    if (camHit.collider.gameObject == gameObject)
                    {
                        m_onClick.Invoke();
                        if (m_triggerOnce)
                            m_triggered.RuntimeValue = true;
                    }
                }
            }
        }
    }

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            var mousePosition = Input.mousePosition;
            touchPosition = new Vector2(mousePosition.x, mousePosition.y);
            return true;
        }
#else
        if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }
#endif

        touchPosition = Vector2.zero;
        return false;
    }

}
