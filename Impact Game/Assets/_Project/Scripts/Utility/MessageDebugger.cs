﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageDebugger : MonoBehaviour
{
    [TextArea(4, 50)]
    public string m_message;

    public void ShowMessage(string message)
    {
        Debug.LogWarning(gameObject.name + ": " + message, gameObject);
    }

    public void ShowMessage()
    {
        Debug.LogWarning(gameObject.name + ": " + m_message, gameObject);
    }
}
