﻿
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Clickable : MonoBehaviour, IPointerDownHandler
{

    [SerializeField] private UnityEvent onClick;
    private void OnMouseDown()
    {
        //onClick.Invoke();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        onClick.Invoke();
    }
}
