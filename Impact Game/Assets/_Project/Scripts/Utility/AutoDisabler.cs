﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDisabler : MonoBehaviour
{
    [SerializeField] private bool m_notInEditor;

    // Start is called before the first frame update
    void Awake()
    {
#if UNITY_EDITOR
        if(!m_notInEditor)
#endif
        gameObject.SetActive(false);
    }
}