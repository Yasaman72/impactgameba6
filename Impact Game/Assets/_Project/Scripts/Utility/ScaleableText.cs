﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScaleableText : MonoBehaviour
{
    [SerializeField] private bool m_dontSetInitialSize;
    [SerializeField] private float m_initialSize;


    public TextMeshProUGUI GetTextComponent()
    {
        if (m_initialSize == 0)
        {
            m_initialSize = gameObject.GetComponent<TextMeshProUGUI>().fontSize;
        }

        if (!m_dontSetInitialSize)
        {
            gameObject.GetComponent<TextMeshProUGUI>().fontSize = m_initialSize;
        }

        return GetComponent<TextMeshProUGUI>();
    }
    
}
