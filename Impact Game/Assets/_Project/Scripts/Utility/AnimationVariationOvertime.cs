﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AnimationVariationOvertime : MonoBehaviour
{
    [SerializeField] private Animator m_animator;
    [SerializeField] private string m_triggerParameter;
    [SerializeField] private string m_indexParameter;
    [SerializeField] private int m_varCount;
    [SerializeField] private float m_minInterval;
    [SerializeField] private float m_maxInterval;

    void OnEnable()
    {
        StartCoroutine(nameof(PlayVariation));
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator PlayVariation()
    {
        while (gameObject.activeInHierarchy)
        {
            //Debug.Log("setting the animation var");
            m_animator.SetTrigger(m_triggerParameter);
            m_animator.SetInteger(m_indexParameter, Random.Range(0, m_varCount));

            yield return new WaitForSeconds(Random.Range(m_minInterval, m_maxInterval));
        }
    }
}
