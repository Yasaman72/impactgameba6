﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteOnOtherPlatforms : MonoBehaviour
{
#if !UNITY_EDITOR
    private void Start()
    {
        Destroy(gameObject);
    }
#endif
}
