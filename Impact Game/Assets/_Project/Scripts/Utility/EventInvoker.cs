﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventInvoker : MonoBehaviour
{
    [SerializeField] private UnityEvent m_events;

    public void InvokeEvent()
    {
        m_events.Invoke();
    }
}
