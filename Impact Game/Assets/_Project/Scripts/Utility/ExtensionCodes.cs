﻿namespace Utility
{
    public static class ExtensionCodes
    {
        /// <summary>
        /// Remaps a float number from a range to another range
        /// </summary>
        public static float Remap(float value, float from1, float to1, float from2, float to2)
        {
            return to1 + ((to2 - to1) / (from2 - from1)) * (value - from1);
        }
    }
}


