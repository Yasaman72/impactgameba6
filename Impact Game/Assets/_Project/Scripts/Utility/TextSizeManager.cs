﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TextSizeManager : MonoBehaviour
{
    public static TextSizeManager Instance;

    [SerializeField] private FloatVariable textSizeMultiplierVariable;


    private List<ScaleableText> _textsList;
    private Dictionary<TextMeshProUGUI, float> _textsDictionary;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        textSizeMultiplierVariable.AddListener(InitTexts);

        _textsDictionary = new Dictionary<TextMeshProUGUI, float>();
        Invoke(nameof(LoadOptionState), 0.5f);
        Invoke(nameof(InitTexts), 1f);
    }

    private void OnDestroy()
    {
        textSizeMultiplierVariable.RemoveListener(InitTexts);
    }

    private void LoadOptionState()
    {
        float fontSize = 1;
        if (PlayerPrefs.HasKey("SetFontSize"))
        {
            fontSize = PlayerPrefs.GetFloat("SetFontSize");
        }
        else
        {
            PlayerPrefs.SetFloat("SetFontSize", 1);
        }

        textSizeMultiplierVariable.RuntimeValue = fontSize;
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetFloat("SetFontSize", textSizeMultiplierVariable.RuntimeValue);
    }

    private void InitTexts()
    {
        _textsList = Resources.FindObjectsOfTypeAll<ScaleableText>().ToList();

        foreach (ScaleableText text in _textsList)
        {
            TextMeshProUGUI temp = text.GetTextComponent();
            if (!_textsDictionary.ContainsKey(temp))
                _textsDictionary.Add(temp, temp.fontSize);
        }

        SetFontSize();
    }


    private void SetFontSize()
    {
        if (textSizeMultiplierVariable == null) return;
        if (_textsDictionary != null && _textsDictionary.Count != 0)
        {
            foreach (KeyValuePair<TextMeshProUGUI, float> text in _textsDictionary)
            {
                text.Key.fontSize = text.Value * textSizeMultiplierVariable.RuntimeValue;
            }
        }
    }

    public void NewSceneLoaded()
    {
        InitTexts();
    }
}