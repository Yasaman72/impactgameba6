﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LineConnector : MonoBehaviour
{
    [SerializeField] private LineRenderer m_line;
    [SerializeField] private Transform m_startPoint, m_endPoint;

    private void Update()
    {
        if (m_line == null || m_startPoint == null || m_endPoint == null) return;

        m_line.SetPosition(0, m_startPoint.position);
        m_line.SetPosition(1, m_endPoint.position);
    }
}
