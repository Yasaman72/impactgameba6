﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAnimationFromRandomTime : MonoBehaviour
{
    [SerializeField] private Animator m_animator;

    void Start()
    {
        float randomTime = ((int)(Random.Range(0f, 1f) * 100)) / 100f;
        Debug.Log("time: " + randomTime);
        m_animator.Play("Env_Lake_LilyWater", 0, 0.5f);
    }

}
