﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;


public class DragableUi : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    public enum DraggableItemType
    {
        Hammer,
        QuestionMark,
        IntractableItem
    }
    
    [SerializeField] private DraggableItemType itemType;
    [SerializeField] public bool canDrag = true;
    [SerializeField, ReadOnly] private bool dragPossible;
    [SerializeField] private Vector3 holdOffset;

    [Space(10)]
    [SerializeField] private float holdTime = 1;
    [SerializeField] private Image holdFiller;
    [SerializeField] private Vector3 fillerPosOffset;
    
    public delegate void OnDragChange();
    public event OnDragChange onDragEnd;
    public event OnDragChange onDragStart;
    public event OnDragChange onDragging;

    private Camera mainCam;

    private void Awake()
    {
        mainCam = Camera.main;
        if (holdFiller != null) holdFiller.gameObject.SetActive(false);
        canDrag = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!canDrag) return;
        
        if (holdFiller != null) holdFiller.gameObject.SetActive(true);
        StartCoroutine(Holding());
        switch (itemType)
        {
            case DraggableItemType.Hammer:
                SFXManager.Instance.PlayHammerPickUpSound();
                break;
            case DraggableItemType.QuestionMark:
                SFXManager.Instance.PlayHammerPickUpSound();
                break;
            case DraggableItemType.IntractableItem:
                SFXManager.Instance.PlayItemPickUpSound();
                break;
        }
        
    }

    IEnumerator Holding()
    {
        float timer = 0;
        // while ((timer + Time.deltaTime) < holdTime)
        // {
        //     timer += Time.deltaTime;
        //
        //     if (holdFiller != null)
        //     {
        //         holdFiller.fillAmount = timer / holdTime;
        //         holdFiller.gameObject.transform.position = Input.mousePosition + fillerPosOffset;
        //     }
        //
        //     yield return new WaitForFixedUpdate();
        // }

        DragStart();

        yield return null;
    }

    private void DragStart()
    {
        if (holdFiller != null) holdFiller.gameObject.SetActive(false);
        if (onDragStart != null) onDragStart.Invoke();
        
        dragPossible = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (dragPossible)
        {
            Vector3 mousePos = mainCam.ScreenToWorldPoint(Input.mousePosition) + holdOffset;
            transform.position = mousePos;
            if (onDragging != null) onDragging.Invoke();
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        dragPossible = false;

        StopAllCoroutines();
        if (holdFiller != null) holdFiller.gameObject.SetActive(false);

        if (onDragEnd != null) onDragEnd.Invoke();
    }
}