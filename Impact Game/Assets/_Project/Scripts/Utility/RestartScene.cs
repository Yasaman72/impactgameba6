﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class RestartScene : MonoBehaviour
{
    [SerializeField]
    private UnityEvent onRestart;

    public void RestartCurrentScene()
    {
        onRestart.Invoke();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
