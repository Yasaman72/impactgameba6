﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAnimatorSpeed : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private bool setSpeedOnEnable;
    [SerializeField] private string speedParameter;
    [SerializeField] private float speed;


    private void onEnable()
    {
        if (setSpeedOnEnable)
        {
            SetAnimationSpeed(speed);
        }
    }

    public void SetAnimationSpeed(float speedValue)
    {
        animator.SetFloat(speedParameter, speedValue);
    }
}
