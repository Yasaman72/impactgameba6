﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GeneralUnityEventInvoker : MonoBehaviour
{
    [SerializeField] private UnityEvent eventToRaise;

    public void RaiseEvent()
    {
        eventToRaise.Invoke();
    }
}
