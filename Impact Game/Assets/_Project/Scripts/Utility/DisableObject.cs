﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableObject : MonoBehaviour
{
    [SerializeField] private GameObject m_objectToDisable;

    public void Disable()
    {
        if (m_objectToDisable == null)
        {
            gameObject.SetActive(false);
        }
        else
        {
            m_objectToDisable.SetActive(false);
        }
    }
}
