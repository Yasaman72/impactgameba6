﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "IntVariable", menuName = "Custom Framework/Variables/Int", order = 1)]
public class IntVariable : ScriptableVar<int>
{
    public void AddNewValue(int newAmount)
    {
        RuntimeValue = RuntimeValue + newAmount;
    }

    public override void SaveValue(int value)
    {
        string id = playerPrefId +"_"+ GetInstanceID();

        if (_debug)
        {
            Debug.Log("saved data for: " + id);
        }
        PlayerPrefs.SetInt(id, value);
    }

    protected override int GetValue()
    {
        string id = playerPrefId +"_"+ GetInstanceID();

        if (_debug)
        {
            Debug.Log("getting data for: " + id);
        }
        return PlayerPrefs.GetInt(id);
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(IntVariable)), CanEditMultipleObjects]
public class Intvar : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GUILayout.Space(30);

        IntVariable myScript = (IntVariable) target;
        if (GUILayout.Button("Delete playerPref Key"))
        {
            myScript.DeletePlayerPrefKey();
        }
    }
}

#endif