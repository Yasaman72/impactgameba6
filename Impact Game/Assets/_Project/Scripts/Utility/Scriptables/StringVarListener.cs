﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StringVarListener : MonoBehaviour
{
    public StringVariable Variable;
    public StringVarEvent onValueChangeString;

    private void OnEnable()
    {
        Variable.AddListener(OnEventRaised);
    }

    private void OnDisable()
    {
        Variable.RemoveListener(OnEventRaised);
    }

    public void OnEventRaised()
    {
        onValueChangeString.Invoke(Variable.RuntimeValue);
    }
}

[System.Serializable]
public class StringVarEvent : UnityEvent<string>
{
}
