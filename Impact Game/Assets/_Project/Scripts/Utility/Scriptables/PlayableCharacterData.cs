﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player Control Data", menuName = "ScriptableObjects/PlayableCharacterData", order = 1)]
public class PlayableCharacterData : ScriptableObject
{
    public string horizontalAxis;
    public string veticalAxis;
    [Space]
    public KeyCode shootKey;

    [Header("PS4")]
    public string ps4HorizontalAxisLeft;
    public string ps4VerticalAxisLeft;
    [Space]
    public string ps4HorizontalAxisRight;
    public string ps4VerticalAxisRight;
    [Space]
    public string ps4ShootKey;
}
