﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "BoolVariable", menuName = "Custom Framework/Variables/Bool", order = 1)]
public class BoolVariable : ScriptableVar<bool>
{
    public void SetTrue()
    {
        RuntimeValue = true;
    }

    public void SetFalse()
    {
        RuntimeValue = false;
    }

    public void Toggle()
    {
        RuntimeValue = !RuntimeValue;
    }

    public override void SaveValue(bool value)
    {
        string id = playerPrefId +"_"+ GetInstanceID();
        var saveInt = value ? 1 : 0;

        PlayerPrefs.SetInt(id, saveInt);
        if (_debug)
        {
            Debug.Log("saved data for: " + id + " value: " + saveInt);
        }
    }

    protected override bool GetValue()
    {
        string id = playerPrefId +"_"+ GetInstanceID();
        if (_debug)
        {
            Debug.Log("gettnig data for: " + id + " value : " + PlayerPrefs.GetInt(id));
        }

        if (PlayerPrefs.HasKey(id))
        {
            return PlayerPrefs.GetInt(id) == 1;
        }
        else
        {
            RuntimeValue = _initialValue;
            return GetValue();
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(BoolVariable)), CanEditMultipleObjects]
public class BoolVar : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GUILayout.Space(30);

        BoolVariable myScript = (BoolVariable) target;
        if (GUILayout.Button("Delete playerPref Key"))
        {
            myScript.DeletePlayerPrefKey();
        }
    }
}

#endif