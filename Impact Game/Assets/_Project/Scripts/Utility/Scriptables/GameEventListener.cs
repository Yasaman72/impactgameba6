﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventListener : MonoBehaviour
{
    public GameEvent Event;
    public UnityEvent response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDestroy()
    {
        Event.UnregisteredListener(this);
    }
    private void OnDisable()
    {
        Event.UnregisteredListener(this);
    }

    public void OnEventRaised()
    {
        response.Invoke();
    }
}
