﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetGameObjectVar : MonoBehaviour
{
    [SerializeField] private GameobjectVariable m_objectVariable;
    [SerializeField] private GameObject m_objectToSetTo;

    private void OnEnable()
    {
        m_objectVariable.RuntimeValue = m_objectToSetTo;
    }
}
