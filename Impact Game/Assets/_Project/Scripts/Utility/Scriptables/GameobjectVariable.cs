﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameobjectVariable", menuName = "Custom Framework/Variables/Gameobject", order = 1)]
public class GameobjectVariable : ScriptableVar<GameObject>
{
}
