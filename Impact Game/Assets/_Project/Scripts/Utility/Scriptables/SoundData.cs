﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "soundData", menuName = "Custom Framework/Sound Data")]
public class SoundData : ScriptableObject
{
    [Range(0,1)]
    public float volume = 0.5f;
    public GameEvent gameEvent;
    public AudioClip[] audioClips;

    void PlaySound()
    {
        SoundManager.Instance._audioSource.clip = audioClips[Random.Range(0, audioClips.Length)];
        SoundManager.Instance._audioSource.volume = volume;
        SoundManager.Instance._audioSource.Play();
    }

    public void InitData()
    {
        gameEvent.RegisterFunction(PlaySound);
    }
}
