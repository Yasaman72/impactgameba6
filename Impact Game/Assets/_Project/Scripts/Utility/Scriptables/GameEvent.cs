﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "GameEvent", menuName = "Custom Framework/GameEvent", order = 1)]
public class GameEvent : ScriptableObject
{
    private List<GameEventListener> _listeners = new List<GameEventListener>();
    private List<UnityAction> _events = new List<UnityAction>();

    [SerializeField] private bool m_debug;
    public UnityEvent onEventRaise;

    public void Raise()
    {
        foreach (GameEventListener gameEv in _listeners.ToList())
        {
            gameEv.OnEventRaised();
        }

        foreach (UnityAction gameEv in _events.ToList())
        {
            gameEv();
        }

        if (m_debug)
        {
            Debug.Log("raised event: " + name);
        }

        onEventRaise.Invoke();
    }

    public void RegisterListener(GameEventListener listener)
    {
        _listeners.Add(listener);
    }

    public void UnregisteredListener(GameEventListener listener)
    {
        _listeners.Remove(listener);
    }

    public void RegisterFunction(UnityAction func)
    {
        if (_events.Contains(func)) return;
        _events.Add(func);
    }


    public void RemoveFunction(UnityAction func)
    {
        if (_events.Contains(func))
            _events.Remove(func);
    }

    private void OnEnable()
    {
        _listeners.Clear();
    }

    private void OnDestroy()
    {
        _events.Clear();
        _listeners.Clear();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(GameEvent))]
public class GameEventEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GUILayout.Space(30);

        GameEvent myScript = (GameEvent) target;
        if (GUILayout.Button("Raise Event"))
        {
            myScript.Raise();
        }
    }
}

#endif