﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    private void FixedUpdate()
    {
        if (Camera.main != null)
        {
            Transform camera = Camera.main.transform;

            transform.LookAt(camera.position, camera.rotation * Vector3.up);
        }
    }
}
