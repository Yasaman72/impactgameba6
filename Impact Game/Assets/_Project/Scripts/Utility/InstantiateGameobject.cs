﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class InstantiateGameobject : MonoBehaviour
{
    [SerializeField] private instantiateInfo[] m_InstantiateInfo;

    [System.Serializable]
    public class instantiateInfo
    {
        public GameObject m_objectToInstantiate;
        public Transform m_parent;
        public Transform m_positionRef;
        public Transform m_rotationRef;
        public Vector3 m_position;
        public Vector3 m_rotation;
        public Vector3 m_scale = Vector3.one;

        public void SetData()
        {
            if (m_positionRef != null)
            {
                m_position = m_positionRef.position;
            }

            if (m_rotationRef != null)
            {
                m_rotation = m_rotationRef.rotation.eulerAngles;
            }
        }
    }

    public void instantiate(int index)
    {
        Quaternion rotation = Quaternion.identity;
        rotation.eulerAngles = m_InstantiateInfo[index].m_rotation;
        m_InstantiateInfo[index].SetData();
        GameObject obj;

        if (m_InstantiateInfo[index].m_parent == null)
        {
            obj = Instantiate(m_InstantiateInfo[index].m_objectToInstantiate, m_InstantiateInfo[index].m_position, rotation);
        }
        else
        {
             obj = Instantiate(m_InstantiateInfo[index].m_objectToInstantiate, m_InstantiateInfo[index].m_position, rotation, m_InstantiateInfo[index].m_parent);
        }

        obj.transform.localScale = m_InstantiateInfo[index].m_scale;
    }
}
