﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactiveGameObj : MonoBehaviour
{
    public void MakeDeactive()
    {
        gameObject.SetActive(false);
    }

    public void MakeActive()
    {
        gameObject.SetActive(true);
    }
}
