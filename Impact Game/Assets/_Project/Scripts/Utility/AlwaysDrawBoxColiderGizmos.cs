﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysDrawBoxColiderGizmos : MonoBehaviour
{
    [SerializeField] private bool show = true;
    [SerializeField] private BoxCollider boxCollider;
    [SerializeField] private Color color = Color.yellow;

    private void OnDrawGizmos()
    {
        if (show && boxCollider != null)
        {
            Gizmos.matrix = boxCollider.transform.localToWorldMatrix;
            Gizmos.color = color;
            Gizmos.DrawWireCube(Vector3.zero + boxCollider.center, boxCollider.size);
        }
    }
}
