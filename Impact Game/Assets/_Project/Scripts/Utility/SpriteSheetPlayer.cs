﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SpriteSheetPlayer : MonoBehaviour
{
    [SerializeField] private Sprite[] sprites;
    [SerializeField] private float animationInterval = 0.2f;
    [SerializeField] private bool playOnce = false ;

    private Image image;
    private SpriteRenderer spriteRenderer;
    
    private bool hasImageRenderer = false;
    private float timer;
    private int currentSpriteId = 0;

    void OnEnable()
    {
        if (sprites == null || sprites.Length == 0) return;
        
        image = GetComponent<Image>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        currentSpriteId = Random.Range(0, sprites.Length);
        
        if (image != null)
            hasImageRenderer = true;
        else
            hasImageRenderer = false;

        if (hasImageRenderer)
        {
            image.sprite = sprites[currentSpriteId];
        }
        else
        {
            spriteRenderer.sprite = sprites[currentSpriteId];
        }


        StartCoroutine(UpdateSprite());
    }
    
    IEnumerator UpdateSprite()
    {
        while (true)
        {
            yield return new WaitForSeconds(animationInterval);

            if (hasImageRenderer)
            {
                image.sprite = sprites[currentSpriteId];
            }
            else
            {
                spriteRenderer.sprite = sprites[currentSpriteId];
            }

            currentSpriteId++;
            if (currentSpriteId > sprites.Length - 1)
            {
                currentSpriteId = 0;
                if (playOnce)
                {
                    spriteRenderer.sprite = null;
                    break;
                }
            }
        }
    }
}