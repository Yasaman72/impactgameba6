﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class RotateTowardCenter : MonoBehaviour
{
    private Quaternion _originalRotation;
    public GameObject _Center;

    private void Start()
    {
        if (_Center == null)
            _Center = GameObject.FindGameObjectWithTag("Center");

        _originalRotation = gameObject.transform.rotation;

        transform.LookAt(_Center.transform.position,
            _Center.transform.rotation * Vector3.up);

        gameObject.transform.eulerAngles = new Vector3(
             _originalRotation.eulerAngles.x,
            gameObject.transform.localEulerAngles.y -90,
            _originalRotation.eulerAngles.z);
    }
}
