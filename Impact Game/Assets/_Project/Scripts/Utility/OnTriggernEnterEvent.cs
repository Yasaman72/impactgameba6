﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTriggernEnterEvent : MonoBehaviour
{
    [SerializeField] private string m_otherObjTag;
    [SerializeField] private UnityEvent m_event;

    private void OnTriggerEnter(Collider other)
    {
        InvokeEvent(other.gameObject);
    }

    private void InvokeEvent(GameObject otherObj)
    {
        if (!String.IsNullOrEmpty(m_otherObjTag))
        {
            if (otherObj.CompareTag(m_otherObjTag))
            {
                m_event.Invoke();
            }
        }
        else
        {
            m_event.Invoke();
        }
    }
}
