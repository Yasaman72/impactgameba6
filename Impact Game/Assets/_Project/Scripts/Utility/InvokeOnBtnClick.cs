﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class InvokeOnBtnClick : MonoBehaviour
{
    private Button m_button;

    private void Start()
    {
        m_button = gameObject.GetComponent<Button>();
    }

    public void onButtonClick()
    {
        m_button.onClick.Invoke();
    }
}
