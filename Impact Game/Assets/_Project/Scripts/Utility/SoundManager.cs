﻿using System;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance; 
    [SerializeField] private SoundData[] m_soundData; 
    [HideInInspector] public AudioSource _audioSource;

    private void Awake()
    {
        Instance = this;
        _audioSource = gameObject.GetComponent<AudioSource>();
    }

    private void Start()
    {
        foreach (SoundData soundData in m_soundData)
        {
            soundData.InitData();
        }
    }
}
