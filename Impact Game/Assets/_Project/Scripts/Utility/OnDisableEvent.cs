﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class OnDisableEvent : MonoBehaviour
{
    [SerializeField] private UnityEvent m_disableEvent;

    private void OnDisable()
    {
        m_disableEvent.Invoke();
    }
}
