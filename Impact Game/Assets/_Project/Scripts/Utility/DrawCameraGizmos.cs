﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawCameraGizmos : MonoBehaviour
{
    [SerializeField] private Camera camera;

    private void OnDrawGizmos()
    {
        if (camera != null)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(camera.transform.position, new Vector3(camera.transform.position.x,
            camera.transform.position.y,
            camera.transform.position.z + camera.farClipPlane));
        }
    }
}
