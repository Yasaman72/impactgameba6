﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyGameObject : MonoBehaviour
{
    [SerializeField] private GameObject m_objectToCDestroy;

    public void DestroyObj()
    {
        if (m_objectToCDestroy != null)
        {
            Destroy(m_objectToCDestroy);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
