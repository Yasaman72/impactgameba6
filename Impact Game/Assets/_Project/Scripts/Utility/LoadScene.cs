﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    [SerializeField] private SceneReference m_sceneToLoad;
    [SerializeField] private LoadSceneMode m_loadSceneMode;
    [SerializeField] private SceneReference[] m_moreScenesToLoad;
    [SerializeField] private LoadSceneMode m_loadSceneModeMoreScenes;
    [Space]
    [SerializeField] private bool m_invoke;
    [SerializeField] private float m_unloadAfterTime;

    private void Start()
    {
        if (!m_invoke) return;
        Invoke(nameof(LoadLevel), m_unloadAfterTime);
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene(m_sceneToLoad, m_loadSceneMode);
        foreach(var scene in m_moreScenesToLoad)
        {
            SceneManager.LoadScene(scene, m_loadSceneModeMoreScenes);
        }
    }
}
