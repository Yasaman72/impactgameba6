﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleHelper : MonoBehaviour
{
    [SerializeField] private ParticleSystem particleSystem;

    public void PlayParticle()
    {
        particleSystem.Play();
    }
}
