﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingTexture : MonoBehaviour
{
    public bool pingPong;
    public float pingPongRange;
    [Space]
    public bool moveOut;
    public bool invert;
    public bool makeTransparent;
    public float colorSpeed;
    [Space]
    public Vector2 Scroll = new Vector2(0.05f, 0.05f);
    Vector2 Offset = new Vector2(0f, 0f);

    private Material material;
    private void Start()
    {
        material = transform.GetComponent<Renderer>().material;
    }

    void Update()
    {
        Offset += Scroll * Time.deltaTime;
        material.SetTextureOffset("_MainTex", Offset);

        if (pingPong)
        {
            if (material.GetTextureOffset("_MainTex").y >= pingPongRange ||
                material.GetTextureOffset("_MainTex").y <= -pingPongRange)
            {
                Scroll *= -1;
            }
        }
        else if (moveOut)
        {
            if (invert)
            {
                if (material.GetTextureOffset("_MainTex").y >= 1)
                {
                    Offset = new Vector2(0f, -1f);
                    material.SetTextureOffset("_MainTex", new Vector2(0f, -1f));
                    material.SetColor("_Color", Color.white);
                }
            }
            else
            {
                if (material.GetTextureOffset("_MainTex").y <= -1)
                {
                    Offset = new Vector2(0f, 1f);
                    material.SetTextureOffset("_MainTex", new Vector2(0f, 1f));
                    material.SetColor("_Color", Color.white);
                }
            }
        }
        if (makeTransparent)
        {
            Color newColor = Color.Lerp(material.GetColor("_Color"), Color.clear, Time.deltaTime * colorSpeed);
            material.SetColor("_Color", newColor);
            //if (material.GetColor("_Color").a <= 0)
            //{
            //    material.SetColor("_Color", Color.white);
            //}
        }
    }
}
