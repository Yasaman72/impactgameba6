﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

public class ChangeMaterialTransparency : MonoBehaviour
{
    //[SerializeField] private BoolVariable m_isScenePlaced;

    [SerializeField] private MeshRenderer[] m_renderer;
    [SerializeField] private float m_fadeTime;
    [SerializeField] private float m_initialTransparency;
    [SerializeField] private GameObject m_ObjectToDeactivate;
    [SerializeField] private GameObject[] m_ObjectsToDeactivateAtStart;

    private void Start()
    {
        FadeIn();
    }

    private void OnEnable()
    {
        FadeIn();
    }

    private void SetMaterialsTransparency(float alpha)
    {
        foreach (var renderer in m_renderer)
        {
            Material material = renderer.material;
            Color color = material.GetColor("_Color");
            if (color.a == alpha) return;
            Color transParentColor = new Color(color.r, color.g, color.b, alpha);
            material.SetColor("_Color", transParentColor);
        }
    }

    public void FadeIn()
    {
        if (!this.isActiveAndEnabled) return;
        SetMaterialsTransparency(0);
        StopAllCoroutines();
        StartCoroutine(Fade(m_initialTransparency, false));
    }
    public void FadeOut(bool deactivateAtEnd)
    {
        if (!this.isActiveAndEnabled) return;
        SetMaterialsTransparency(m_initialTransparency);
        StopAllCoroutines();
        StartCoroutine(Fade(0, deactivateAtEnd));
    }


    private float m_passedTime;
    IEnumerator Fade(float fadeIn, bool deactivate)
    {

        foreach (var obj in m_ObjectsToDeactivateAtStart)
        {
            obj.SetActive(!deactivate);
        }

        while (Math.Abs(m_renderer[0].material.GetColor("_Color").a - fadeIn) > 0.01f)
        {
            foreach (var renderer in m_renderer)
            {
                Material material = renderer.material;
                Color color = material.GetColor("_Color");
                float alpha = Mathf.Lerp(color.a, fadeIn, m_passedTime / m_fadeTime);
                m_passedTime = Time.deltaTime;
                SetMaterialsTransparency(alpha);
            }

            yield return null;
        }

        SetMaterialsTransparency(fadeIn);

        m_ObjectToDeactivate.SetActive(!deactivate);
    }
}
