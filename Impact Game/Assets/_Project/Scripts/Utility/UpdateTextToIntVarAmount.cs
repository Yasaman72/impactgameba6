﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UpdateTextToIntVarAmount : MonoBehaviour
{
    [SerializeField] private IntVariable intVar;
    [SerializeField] private TextMeshProUGUI text;

    private void OnEnable()
    {
        intVar.AddListener(AmountChanged);
    }
    private void OnDisable()
    {
        intVar.AddListener(AmountChanged);
    }

    private void AmountChanged()
    {
        text.text = intVar.RuntimeValue.ToString();
    }
}
