﻿using System.Collections;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class PumpManager : GeneralInteractaboleManager
{
    [Header("Item Specific")]
    [SerializeField] private GameEvent swipeDown;
    [SerializeField] private GameEvent swipeUp;

    [Space]
    [SerializeField] private Transform leftPart;
    [SerializeField] private Transform rightPart;
    [SerializeField] private float leftPartMovePerInput;
    [SerializeField] private float upMostPosition;

    [Space]
    [SerializeField] private float leftPartMoveDownPerSecond;
    [SerializeField] private float lowMostPosition;

    [Header("neon mode")]
    [SerializeField] private float neonModeMinPos;
    [SerializeField] private float neonModeMaxPos;
    [SerializeField] private int secondToAddToCollectiveNeonTimeOnComplete = 1;

    [Space]
    [SerializeField, ReadOnly] private bool engineIsReady;
    [SerializeField, ReadOnly] private bool shouldSwipeUp;
   
    [Header("Interaction Indicator")]
    [SerializeField] private float showIndicatorAfterSeconds = 2;
    [SerializeField] private Transform indicatorPos;

    private Vector3 newPositionMoveUp;
    private Vector3 newPositionDown;
    private Tween moveDownTween;
    private float tweenLastTime;

    private void OnEnable()
    {
        base.OnEnable();

        swipeDown.RegisterFunction(SetupTheEngine);
        swipeUp.RegisterFunction(OnSwipeUp);

        StartCoroutine(ZigZagEffectLoop());
        StartCoroutine(SparkleEffectLoop());
    }

    private void OnDisable()
    {
        base.OnDisable();

        swipeDown.RemoveFunction(SetupTheEngine);
        swipeDown.RemoveFunction(OnSwipeDown);
        swipeUp.RemoveFunction(OnSwipeUp);
        
        StopAllCoroutines();
    }

    private void SetupTheEngine()
    {
        if (!isFocused) return;
        if (engineIsReady) return;

        swipeDown.RegisterFunction(OnSwipeDown);
        engineIsReady = true;
        shouldSwipeUp = true;
        // todo: move the right part down
        rightPart.DOLocalMove(rightPart.localPosition - (Vector3.up * 2), .5f);
        EngineMoveDown();
        
        InteractionIndicatorManager.Instance.ShowIndicator(Interaction.SwipeUp, indicatorPos,
                                                           showIndicatorAfterSeconds, 3); 
    }

    private void OnSwipeUp()
    {
        if (!isFocused) return;
        if (!engineIsReady) return;

        if (!shouldSwipeUp) return;
        shouldSwipeUp = false;

        rightPart.DOLocalMove(rightPart.localPosition + Vector3.up, .5f);

        MoveUpLeftPart();
        
        InteractionIndicatorManager.Instance.ShowIndicator(Interaction.SwipeDown, indicatorPos,
                                                           showIndicatorAfterSeconds, 3); 
    }

    private void OnSwipeDown()
    {
        if (!isFocused) return;
        if (!engineIsReady) return;

        if (shouldSwipeUp) return;
        shouldSwipeUp = true;

        rightPart.DOLocalMove(rightPart.localPosition - Vector3.up, .5f);
        SFXManager.Instance.PlayPumpDownSound();

        MoveUpLeftPart();
        
        InteractionIndicatorManager.Instance.ShowIndicator(Interaction.SwipeUp, indicatorPos,
                                                           showIndicatorAfterSeconds, 3); 
    }

    public override void FocusItem()
    {
        if (isInFocusMode.RuntimeValue || isFocused) return;
        
        if (threeLinesEffect.activeInHierarchy)
            threeLinesEffect.SetActive(false);
        
        base.FocusItem();
        InteractionIndicatorManager.Instance.ShowIndicator(Interaction.SwipeDown, indicatorPos,
                                                           showIndicatorAfterSeconds, -1); 
        
    }

    public override void UnfocusItem()
    {
        if (!threeLinesEffect.activeInHierarchy)
            threeLinesEffect.SetActive(true);
        
        base.UnfocusItem();
        InteractionIndicatorManager.Instance.CancelEnabling();
    }

    private void MoveUpLeftPart()
    {
        moveDownTween.Kill();
        EngineMoveDown();

        if (leftPart.localPosition.y + leftPartMovePerInput >= upMostPosition)
        {
            Debug.Log("on the most top position");

            OverdoneStateEnter();
            return;
        }

        newPositionMoveUp = new Vector3(leftPart.localPosition.x, leftPart.localPosition.y + leftPartMovePerInput,
                                  leftPart.localPosition.z);
        leftPart.DOLocalMove(newPositionMoveUp, 1);

        CheckNeon();
    }

    private void CheckNeon()
    {
        if (leftPart.localPosition.y > neonModeMinPos && leftPart.localPosition.y < neonModeMaxPos)
        {
            OnNeonModeEnter();
        }
        else
        {
            OnNeonModeExit();
        }
        
    }
    
    private void EngineMoveDown()
    {
        if (leftPart.localPosition.y - leftPartMoveDownPerSecond <= lowMostPosition)
        {
            Debug.Log("on the most down position");
            OverdoneStateEnter();
            return;
        }

        newPositionDown = new Vector3(leftPart.localPosition.x, leftPart.localPosition.y - leftPartMoveDownPerSecond,
                                      leftPart.localPosition.z);
        moveDownTween = leftPart.DOLocalMove(newPositionDown, 1).SetEase(Ease.Linear);
        moveDownTween.onComplete += EngineMoveDown;
        CheckNeon();

    }
    
    private void OverdoneStateEnter()
    {
        
    }

    private void OnNeonModeEnter()
    {
        isInNeonState = true;

        if (Time.time - tweenLastTime >= 1)
        {
            AddToCollectiveNeonTime();
        }
        
        tweenLastTime = Time.time;
    }

    private void AddToCollectiveNeonTime()
    {
        CollectionManager.Instance.AddToCollectiveNeonTime(gameObject, secondToAddToCollectiveNeonTimeOnComplete);
    }

    private void OnNeonModeExit()
    {
        isInNeonState = false;
    }
    
    private IEnumerator ZigZagEffectLoop()
    {
        while (true)
        {
            yield return new WaitForSeconds(3);
            if (!isInNeonState)
            {
                GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.Zigzag);
            }
        }
    }
    
    private IEnumerator SparkleEffectLoop()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            if (isInNeonState)
            {
                GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.Positive);
            }
        }
    }
}