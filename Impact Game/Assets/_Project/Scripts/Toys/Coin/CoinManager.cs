﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

public class CoinManager : GeneralInteractaboleManager
{
    [Header("Item Specific")] [SerializeField]
    private Animator animator;

    [SerializeField] private string flipParameter = "Flip";

    [Space]
    [SerializeField] private GameEvent swipeLeft;

    [SerializeField] private GameEvent swipeRight;
    [SerializeField] private GameEvent swipeUp;

    [Space]
    [SerializeField] private float stopSpinAfterSeconds = 1;

    [SerializeField] private float maxSpeed = 2;

    [Space]
    [SerializeField] private float flipOnIdleAfterSeconds = 1;

    [Header("neglect and neon")]
    [SerializeField] private float thresholdMinSpeed = 1;
    [SerializeField] private float thresholdMaxSpeed = 2;
    [SerializeField] private int amountToAddToCollectiveNeonEachSecond = 1;
    [SerializeField] private GameObject spinLineEffect;

    [Header("Interaction Indicator")]
    [SerializeField] private Interaction flippingInteraction;
    [SerializeField] private float showFlipIndicatorAfterSeconds = 2;
    [SerializeField] private Interaction spinningInteraction;
    [SerializeField] private float showSpinIndicatorAfterSeconds = 2;
    [SerializeField] private Transform tapIndicatorLocation;

    private bool isAddingToNeon;
    private float _currentSpeed;
    private bool isShowingZigZagEffect = false;
    private Coroutine zigzagEffectCoroutine;
    private VisualEffectManager visualEffectManager;

    [ShowInInspector, ReadOnly]
    private float currentSpeed
    {
        get => _currentSpeed;
        set
        {
            _currentSpeed = value;

            if (value > 0.5f && !SFXManager.Instance.isPlayingCoinSound)
            {
                SFXManager.Instance.PlayCoinSpinSound();
            }
            else if (value <= 0.5f && SFXManager.Instance.isPlayingCoinSound)
            {
                SFXManager.Instance.StopCoinSpinSound();
            }

            SetNeonState();
            if (isInNeonState && !isAddingToNeon)
            {
                StartCoroutine(NeonState());
                if (isShowingZigZagEffect)
                {
                    isShowingZigZagEffect = false;
                    StopCoroutine(zigzagEffectCoroutine);
                }
            }
            else
            {
                if (value >= thresholdMaxSpeed && !isShowingZigZagEffect)
                {
                    isShowingZigZagEffect = true;
                    zigzagEffectCoroutine = StartCoroutine(ZigZagEffectLoop());
                }
            }
        }
    }

    private bool spinToLeft = true;

    private void Start()
    {
        base.Start();
        visualEffectManager = spinLineEffect.GetComponent<VisualEffectManager>();
    }

    private void OnEnable()
    {
        base.OnEnable();

        swipeLeft.RegisterFunction(OnSwipeLeft);
        swipeRight.RegisterFunction(OnSwipeRight);
        swipeUp.RegisterFunction(OnSwipeUp);
    }

    private void OnDisable()
    {
        base.OnDisable();

        swipeLeft.RemoveFunction(OnSwipeLeft);
        swipeRight.RemoveFunction(OnSwipeRight);
        swipeUp.RemoveFunction(OnSwipeUp);
    }

    public override void FocusItem()
    {
        if (isInFocusMode.RuntimeValue || isFocused) return;
        base.FocusItem();
        InteractionIndicatorManager.Instance.ShowIndicator(flippingInteraction, tapIndicatorLocation,
                                                           showFlipIndicatorAfterSeconds);
    }

    public void ToggleFlipTheCoin()
    {
        if (!isFocused && !animator.GetBool(flipParameter)) return;

        SFXManager.Instance.PlayCoinFlipSound();

        animator.SetBool(flipParameter, !animator.GetBool(flipParameter));

        if (animator.GetBool(flipParameter))
        {
            Invoke(nameof(FlipOnIdle), flipOnIdleAfterSeconds);
           
            if (threeLinesEffect.activeInHierarchy)
                threeLinesEffect.SetActive(false);
        }
        else
        {
            CancelInvoke(nameof(FlipOnIdle));
            spinLineEffect.SetActive(false);
            threeLinesEffect.SetActive(true);
            GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.CurlyLine);
            
        }

        StopCoroutine(StopSpinGradually());
        currentSpeed = 0;
    }

    private void OnSwipeLeft()
    {
        if (!isFocused) return;

        CancelInvoke(nameof(FlipOnIdle));
        if (!spinToLeft)
            currentSpeed -= currentSpeed / 2;

        spinToLeft = true;
        Spin();

        spinLineEffect.SetActive(true);
        if (visualEffectManager.IsFlipDirectionRight())
        {
            visualEffectManager.FlipSprite();
        }

        CancelInvoke(nameof(DisableSpineLine));
        Invoke(nameof(DisableSpineLine), 0.3f);
    }

    private void OnSwipeRight()
    {
        if (!isFocused) return;

        CancelInvoke(nameof(FlipOnIdle));
        if (spinToLeft)
            currentSpeed -= currentSpeed / 2;

        spinToLeft = false;
        Spin();

        spinLineEffect.SetActive(true);
        if (!visualEffectManager.IsFlipDirectionRight())
        {
            visualEffectManager.FlipSprite();
        }

        CancelInvoke(nameof(DisableSpineLine));
        Invoke(nameof(DisableSpineLine), 0.3f);
    }


    private void OnSwipeUp()
    {
        if (!isFocused) return;

        if (!animator.GetBool(flipParameter))
        {
            ToggleFlipTheCoin();
            InteractionIndicatorManager.Instance.ShowIndicator(spinningInteraction, tapIndicatorLocation, showSpinIndicatorAfterSeconds, 1);
        }
    }

    private void DisableSpineLine()
    {
        if (spinLineEffect.activeInHierarchy)
        {
            spinLineEffect.SetActive(false);
        }
    }

    private void Spin()
    {
        if (currentSpeed < maxSpeed)
            currentSpeed += 0.2f;

        SpinTheCoin(true);

        StopCoroutine(StopSpinGradually());
        CancelInvoke(nameof(StopSpin));
        Invoke(nameof(StopSpin), stopSpinAfterSeconds);
    }

    private void StopSpin()
    {
        if (gameObject.activeInHierarchy)
            StartCoroutine(StopSpinGradually());
    }

    private IEnumerator StopSpinGradually()
    {
        while (currentSpeed > 0)
        {
            currentSpeed -= 0.1f;
            SpinTheCoin(true);
            yield return new WaitForSeconds(0.1f);
        }

        currentSpeed = 0;
        SpinTheCoin(false);
        Invoke(nameof(FlipOnIdle), flipOnIdleAfterSeconds);
    }

    private void FlipOnIdle()
    {
        ToggleFlipTheCoin();
        InteractionIndicatorManager.Instance.DisableIndicator(spinningInteraction);
    }

    private void SpinTheCoin(bool spin)
    {
        if (spin)
        {
            if (spinToLeft)
            {
                animator.SetFloat("SpinSpeed", currentSpeed);
            }
            else
            {
                animator.SetFloat("SpinSpeed", -currentSpeed);
            }
        }
        else
        {
            animator.SetFloat("SpinSpeed", 0);
        }
    }

    private IEnumerator NeonState()
    {
        isAddingToNeon = true;
        int counter = 0;
        GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.Positive);
        while (isInNeonState)
        {
            yield return new WaitForSeconds(1);
            CollectionManager.Instance.AddToCollectiveNeonTime(gameObject, amountToAddToCollectiveNeonEachSecond);
            counter++;
            if (counter % 2 == 0)
            {
                GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.Positive);
            }
        }

        isAddingToNeon = false;
    }

    private IEnumerator ZigZagEffectLoop()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            if (!isInNeonState)
            {
                GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.Zigzag);
            }
        }
    }

    private void SetNeonState()
    {
        isInNeonState = Mathf.Abs(currentSpeed) >= thresholdMinSpeed && Mathf.Abs(currentSpeed) <= thresholdMaxSpeed;
    }
}