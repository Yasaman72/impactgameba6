﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class FlowerManager : GeneralInteractaboleManager
{
    [Header("Item Specific")] public static FlowerManager instance;

    [SerializeField] private float petalScale = 1f;
    [SerializeField] private Transform poolHolder;
    [SerializeField] private int petalPoolCap;
    [SerializeField] private float newPetalGrowInterval = 2;

    [Space] [SerializeField] private int minPetalCount = 3;

    [SerializeField] private int maxPetalCount = 12;
    [SerializeField] private Transform[] petalPositions;
    [AssetsOnly, SerializeField] private GameObject[] petals;

    [Space] [SerializeField] private GameEvent swipeDown;
    [SerializeField] private Animator seedAnimator;

    [Header("Neon State")] [SerializeField]
    private float thresholdMin = 5;

    [SerializeField] private float thresholdMax = 10;
    [SerializeField] private int amountToAddToCollectiveNeonEachSecond = 1;
    [SerializeField] private float showZigZagIntervalTime = 3;

    public List<GameObject> availablePetalsPool = new List<GameObject>();
    public List<GameObject> activePetalsPool = new List<GameObject>();
    public List<int> availablePositions = new List<int>();

    [Header("Interaction Indicator")] [SerializeField]
    private float showIndicatorAfterSeconds = 1;

    [SerializeField] private Transform tapIndicatorPos;
    [SerializeField] private string trimText = "trim the bulb!";
    [SerializeField] private float showingTipWait = 3;
    [SerializeField] private float TipFadeAfter = 3;
    [SerializeField] private GameEvent shakePetals;
    [SerializeField] private float shakeAfter;

    private GameObject newPetal;
    [SerializeField, ReadOnly] private int _createdPetalsCount;
    private bool shouldGrow;
    private bool pickAPetal;

    private int createdPetalsCount
    {
        get => _createdPetalsCount;
        set
        {
            _createdPetalsCount = value;
            CheckNeonState();
        }
    }

    private Coroutine zigzagEffectCoroutine;
    private bool isAddingToNeon;

    private void Awake()
    {
        instance = this;
    }

    public override void FocusItem()
    {
        if (isInFocusMode.RuntimeValue || isFocused) return;
        base.FocusItem();

        if (seedAnimator.GetBool("grow")) return;

        swipeDown.RegisterFunction(Grow);
        InteractionIndicatorManager.Instance.ShowIndicator(Interaction.SwipeDown, transform,
            showIndicatorAfterSeconds);
    }

    public override void UnfocusItem()
    {
        base.UnfocusItem();
        // StopAllCoroutines();

        if (zigzagEffectCoroutine != null)
            StopCoroutine(zigzagEffectCoroutine);

        // seedAnimator.SetBool("grow", false);
        threeLinesEffect.SetActive(true);
        // RemoveAllPetals();
        // swipeDown.RemoveFunction(Grow);
    }

    private void Grow()
    {
        swipeDown.RemoveFunction(Grow);
        MakeAvailablePositionsPool();

        seedAnimator.SetBool("grow", true);
        threeLinesEffect.SetActive(false);

        for (int i = 0; i < minPetalCount; i++)
        {
            AddNewPetal();
        }

        // InteractionIndicatorManager.Instance.ShowIndicator(Interaction.Tap, tapIndicatorPos,
        //                                                    showIndicatorAfterSeconds, 3);
        // TipManager.Instance.ShowTip(trimText, TipFadeAfter, showingTipWait);
        pickAPetal = false;
        Invoke(nameof(ShakePetals), shakeAfter);

        shouldGrow = true;
        StartCoroutine(KeepGrowing());
    }

    private void ShakePetals()
    {
        shakePetals.Raise();
        if (!pickAPetal)
            Invoke(nameof(ShakePetals), shakeAfter);
    }

    void MakeAvailablePositionsPool()
    {
        for (int i = 0; i < petalPositions.Length; i++)
        {
            if (availablePositions.Count == petalPositions.Length)
            {
                availablePositions[i] = i;
            }
            else
            {
                availablePositions.Add(i);
            }
        }
    }

    private IEnumerator KeepGrowing()
    {
        while (shouldGrow)
        {
            yield return new WaitForSeconds(newPetalGrowInterval);
            AddNewPetal();
        }
    }


    private void RemoveAllPetals()
    {
        foreach (var petal in activePetalsPool)
        {
            petal.GetComponent<Collider2D>().enabled = false;
            petal.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        }
    }

    public void MakeActivePool(GameObject petal)
    {
        if (!activePetalsPool.Contains(petal))
            activePetalsPool.Add(petal);
    }

    public void OnPetalPick(int petalIndex, GameObject petal)
    {
        pickAPetal = true;

        petal.transform.SetParent(poolHolder);
        petal.GetComponent<Collider2D>().enabled = false;
        SFXManager.Instance.PlayPetalPluckSound();
        GameVFXManager.instance.SpawnParticleEffect(petal.transform, GameVFXManager.ParticleType.SmallPositive);
    }

    public void OnPetalOutOfScreen(int petalIndex, GameObject petal)
    {
        InteractionIndicatorManager.Instance.CancelEnabling();
        petal.SetActive(false);
        activePetalsPool.Remove(petal);
        availablePetalsPool.Add(petal);
        availablePositions.Add(petalIndex);
        createdPetalsCount--;
    }

    private void AddNewPetal()
    {
        if (createdPetalsCount >= maxPetalCount)
        {
            swipeDown.RegisterFunction(Grow);
            shouldGrow = false;
            seedAnimator.SetBool("grow", false);
            RemoveAllPetals();
            return;
        }

        // if (!isFocused) return;

        newPetal = GetAPetal();
        if (!newPetal) return;
        newPetal.SetActive(false);

        SetupNewPetal(newPetal, createdPetalsCount);
    }

    private void CheckNeonState()
    {
        SetNeonState();


        if (isInNeonState)
        {
            if (!isAddingToNeon)
                StartCoroutine(NeonState());
        }
        else
        {
            if (zigzagEffectCoroutine == null)
                zigzagEffectCoroutine = StartCoroutine(ZigZagEffectLoop());
        }
    }

    private IEnumerator NeonState()
    {
        isAddingToNeon = true;


        while (isInNeonState)
        {
            yield return new WaitForSeconds(1);
            CollectionManager.Instance.AddToCollectiveNeonTime(gameObject, amountToAddToCollectiveNeonEachSecond);
        }

        isAddingToNeon = false;
    }

    private GameObject GetAPetal()
    {
        if (availablePetalsPool.Count > 0)
        {
            var newObj = availablePetalsPool[0];
            availablePetalsPool.RemoveAt(0);
            return newObj;
        }

        if (activePetalsPool.Count < petalPoolCap)
        {
            return Instantiate(petals[Random.Range(0, petals.Length)]);
        }

        Debug.Log("reached the cap of possible petals");
        return null;
    }

    private void SetupNewPetal(GameObject newPetal, int growIndex)
    {
        newPetal.SetActive(true);

        newPetal.GetComponent<FlowerPetal>().SetupNewPetal(availablePositions[0]);
        newPetal.GetComponent<Collider2D>().enabled = isFocused;

        newPetal.transform.SetParent(petalPositions[availablePositions[0]]);
        newPetal.transform.localPosition = Vector3.zero;
        newPetal.transform.localRotation = quaternion.identity;
        newPetal.transform.localScale = Vector3.one * petalScale;
        GameVFXManager.instance.SpawnParticleEffect(newPetal.transform, GameVFXManager.ParticleType.SmallSmoke);

        availablePositions.RemoveAt(0);
        createdPetalsCount++;
        if (!spriteRenderers.Contains(newPetal.GetComponent<SpriteRenderer>()))
            spriteRenderers.Add(newPetal.GetComponent<SpriteRenderer>());

        ChangeSaturation();
    }

    private IEnumerator ZigZagEffectLoop()
    {
        while (true)
        {
            yield return new WaitForSeconds(showZigZagIntervalTime);
            if (!isInNeonState)
            {
                GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.Zigzag);
            }
        }
    }

    private void SetNeonState()
    {
        isInNeonState = createdPetalsCount >= thresholdMin && createdPetalsCount <= thresholdMax;
    }
}