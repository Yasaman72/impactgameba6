﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class FlowerPetal : MonoBehaviour, IPointerDownHandler, IDragHandler
{
    [SerializeField] private Rigidbody2D rb2D;
    [SerializeField] private int petalIndex;
    [Header("shake")]
    [SerializeField] private GameEvent shakePetals;
    [SerializeField] private float shakeDuration = 2;
    [SerializeField] private float shakeStrength = 50;
    [SerializeField] private int shakeVibration = 10;
    [SerializeField] private float shakeRandomness = 100;
    
private Vector3 startPos;
    private Camera mainCam;
    private bool isAttached = true;

    private void Awake()
    {
        mainCam = Camera.main;
    }

    private void OnEnable()
    {
        FlowerManager.instance.MakeActivePool(gameObject);
        shakePetals.RegisterFunction(Shake);
    }

    private void OnDisable()
    {
        shakePetals.RemoveFunction(Shake);
    }

    private void Shake()
    {
        transform.DOShakeRotation(shakeDuration, shakeStrength, shakeVibration, shakeRandomness);
    }

    public void SetupNewPetal(int newIndex)
    {
        petalIndex = newIndex;
        rb2D.bodyType = RigidbodyType2D.Kinematic;
        isAttached = true;
    }

    private void OnBecameInvisible()
    {
        if (FlowerManager.instance.gameObject.activeInHierarchy && gameObject.activeInHierarchy)
        {
            FlowerManager.instance.OnPetalOutOfScreen(petalIndex,gameObject);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (isAttached)
        {
            FlowerManager.instance.OnPetalPick(petalIndex, gameObject);
            isAttached = false;
            rb2D.bodyType = RigidbodyType2D.Dynamic;
        }

        Vector2 cursorPosition = mainCam.ScreenToWorldPoint(Input.mousePosition);
        startPos = new Vector3(cursorPosition.x - transform.localPosition.x,
                               cursorPosition.y - transform.localPosition.y);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 cursorPosition = mainCam.ScreenToWorldPoint(Input.mousePosition);
        gameObject.transform.localPosition = new Vector3(cursorPosition.x - startPos.x, cursorPosition.y - startPos.y);
    }
}