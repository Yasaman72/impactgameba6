﻿using System;
using System.Collections;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using Random = UnityEngine.Random;

public class OrigamiManager : GeneralInteractaboleManager
{
    [Header("Item Specific")]
    [SerializeField] private GameEvent swipeLeft;

    [SerializeField] private GameEvent swipeRight;
    [SerializeField] private GameEvent swipeDown;
    [SerializeField] private GameEvent swipeUp;
    [SerializeField] private GameEvent tap;

    [Space]
    [SerializeField] private int resultsCount;

    [SerializeField] private Animator animator;
    [SerializeField] private string progressParameter = "Progress", resultParameter = "Result Index";

    [Header("neglect and neon")]
    [SerializeField] private float neglectSeconds = 3;

    [SerializeField] private int secondToAddToCollectiveNeonTimeOnComplete = 1;

    [Header("Interaction Indicator")]
    [SerializeField] private float showIndicatorAfterSeconds = 1;

    [SerializeField] private Transform tapIndicatorLocation;

    private origamiState currentState;

    enum origamiState
    {
        base1,
        base2,
        base3,
        base4,
        base5,
        finish
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        swipeRight.RegisterFunction(Progress);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        DeresgiterAll();
    }

    private void DeresgiterAll()
    {
        swipeRight.RemoveFunction(Progress);
        swipeLeft.RemoveFunction(Progress);
        swipeDown.RemoveFunction(Progress);
        swipeUp.RemoveFunction(Progress);
        tap.RemoveFunction(Progress);
    }

    public override void FocusItem()
    {
        if (isInFocusMode.RuntimeValue || isFocused) return;
        base.FocusItem();
        InteractionIndicatorManager.Instance.ShowIndicator(Interaction.SwipeRight, transform,
                                                           showIndicatorAfterSeconds, -1);
    }

    public override void UnfocusItem()
    {
        base.UnfocusItem();
        InteractionIndicatorManager.Instance.DisableAllIndicators();
    }

    private void Progress()
    {
        if (!isFocused)
        {
            if (!threeLinesEffect.activeInHierarchy)
                threeLinesEffect.SetActive(true);
            return;
        }

        if (threeLinesEffect.activeInHierarchy)
            threeLinesEffect.SetActive(false);

        InteractionIndicatorManager.Instance.CancelEnabling();

        int resultIndex = 0;
        switch (currentState)
        {
            case origamiState.base1:
                currentState = origamiState.base2;
                swipeRight.RemoveFunction(Progress);
                swipeLeft.RegisterFunction(Progress);
                InteractionIndicatorManager.Instance.ShowIndicator(Interaction.SwipeLeft, transform,
                                                                   showIndicatorAfterSeconds, -1);
                break;

            case origamiState.base2:
                currentState = origamiState.base3;
                swipeLeft.RemoveFunction(Progress);
                swipeUp.RegisterFunction(Progress);
                InteractionIndicatorManager.Instance.ShowIndicator(Interaction.SwipeUp, transform,
                                                                   showIndicatorAfterSeconds, -1);
                break;

            case origamiState.base3:
                currentState = origamiState.base4;
                swipeUp.RemoveFunction(Progress);
                swipeDown.RegisterFunction(Progress);
                InteractionIndicatorManager.Instance.ShowIndicator(Interaction.SwipeDown, transform,
                                                                   showIndicatorAfterSeconds, -1);
                break;

            case origamiState.base4:
                currentState = origamiState.base5;
                swipeDown.RemoveFunction(Progress);
                tap.RegisterFunction(Progress);
                InteractionIndicatorManager.Instance.ShowIndicator(Interaction.Tap, tapIndicatorLocation,
                                                                   showIndicatorAfterSeconds, -1);
                break;

            case origamiState.base5:
                currentState = origamiState.finish;
                resultIndex = Random.Range(0, resultsCount);
                animator.SetInteger(resultParameter, resultIndex);
                AddToNeonStateTimeOnFinish();
                // InteractionIndicatorManager.Instance.ShowIndicator(Interaction.Tap, tapIndicatorLocation,
                //                                                    showIndicatorAfterSeconds,-1);
                break;

            case origamiState.finish:
                currentState = origamiState.base1;
                swipeRight.RegisterFunction(Progress);
                tap.RemoveFunction(Progress);
                InteractionIndicatorManager.Instance.ShowIndicator(Interaction.SwipeRight, transform,
                                                                   showIndicatorAfterSeconds, -1);
                break;

            default:
                throw new ArgumentOutOfRangeException();
        }

        GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.Smoke);
        GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.Positive);
        SFXManager.Instance.PlayPaperFoldingSound();

        isInNeonState = true;

        StopAllCoroutines();
        if (currentState != origamiState.base1)
        {
            // if (currentState != origamiState.finish)
            StartCoroutine(NeglectCountDown());
        }

        animator.SetTrigger(progressParameter);
    }

    IEnumerator NeglectCountDown()
    {
        yield return new WaitForSeconds(neglectSeconds);
        DeresgiterAll();
        swipeRight.RegisterFunction(Progress);
        currentState = origamiState.base1;
        animator.SetTrigger("Reset");
        GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.Smoke);
        GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.CurlyLine);
        SFXManager.Instance.PlayPoofSound();
        if (!threeLinesEffect.activeInHierarchy)
            threeLinesEffect.SetActive(true);

        isInNeonState = false;

        InteractionIndicatorManager.Instance.DisableAllIndicators();
        InteractionIndicatorManager.Instance.ShowIndicator(Interaction.SwipeRight, transform,
                                                           showIndicatorAfterSeconds, -1);
    }

    private void AddToNeonStateTimeOnFinish()
    {
        CollectionManager.Instance.AddToCollectiveNeonTime(gameObject, secondToAddToCollectiveNeonTimeOnComplete);
    }
}