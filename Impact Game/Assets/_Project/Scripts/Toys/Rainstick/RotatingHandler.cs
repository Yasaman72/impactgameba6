﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class RotatingHandler : MonoBehaviour, IDragHandler, IPointerUpHandler,IEndDragHandler
{
    public bool canRotate;
    
    [SerializeField] private Vector3 holdOffset;
    [SerializeField] private Transform center;
    [SerializeField] private Transform center2;

    private Camera mainCam;
    private Vector3 initialPos;

    private void OnEnable()
    {
        initialPos = transform.localPosition;
        mainCam = Camera.main;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(!canRotate) return;
        
        Vector2 mousePos = mainCam.ScreenToWorldPoint(Input.mousePosition) + holdOffset;
        transform.position = mousePos;

        // Debug.DrawLine(center.position, transform.position);
        var dir = center.position - transform.position;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        center.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        center2.rotation = Quaternion.AngleAxis(-angle, Vector3.forward);
        
        if(!SFXManager.Instance.isPlayingRainStickSpinSound)
            SFXManager.Instance.PlayRainStickSpinSound();
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        SFXManager.Instance.StopRainStickSpinSound();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        transform.DOLocalMove(initialPos, .1f);
    }
}