﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainstickManager : GeneralInteractaboleManager
{
    [Space]
    [SerializeField] private RotatingHandler rotatingHandler;

    [Header("Item specific")]
    [Header("pebble spawning")]
    [SerializeField] private int pebbleSpawnWithEachTap = 1;

    [SerializeField] private GameEvent tap;
    [SerializeField] private GameObject rainstickPebble;
    [SerializeField] private int maxPebbles = 25;
    [SerializeField] private Transform partcleSpawnPoint;
    [SerializeField] private float pebblesScale = 0.6f;

    [Header("Neon and neglect")]
    [SerializeField] private IntVariable collectivePebblesHitCount;

    [SerializeField] private float minHitPerPebbleNeonThreshold = 10;
    [SerializeField] private float maxHitPerPebbleNeonThreshold = 30;
    [SerializeField] private int amountToAddToCollectiveNeonEachSecond = 1;

    [Header("Interaction Indicator")]
    [SerializeField] private float showIndicatorAfterSeconds = 2;

    [SerializeField] private Transform rotateIndicatorPos;
    [SerializeField] private Transform tapIndicatorPos;

    private bool isAddingToNeon;
    private int pebbleCount;

    private Vector3 targetRotation;
    private List<GameObject> presentPebbles = new List<GameObject>();

    private void OnEnable()
    {
        base.OnEnable();

        tap.RegisterFunction(OnTap);
        collectivePebblesHitCount.RuntimeValue = 0;
        collectivePebblesHitCount.AddListener(IsInNeonZone);
    }

    private void OnDisable()
    {
        base.OnDisable();

        tap.RemoveFunction(OnTap);
    }

    public override void FocusItem()
    {
        if (isInFocusMode.RuntimeValue || isFocused) return;
        
        if (threeLinesEffect.activeInHierarchy)
            threeLinesEffect.SetActive(false);
        
        base.FocusItem();
        InteractionIndicatorManager.Instance.ShowIndicator(Interaction.Tap, tapIndicatorPos,
                                                           showIndicatorAfterSeconds, 3);
    }

    public override void UnfocusItem()
    {
        if (!threeLinesEffect.activeInHierarchy)
            threeLinesEffect.SetActive(true);
        base.UnfocusItem();

        rotatingHandler.canRotate = false;
        DestroyAllPebbles();

        InteractionIndicatorManager.Instance.DisableAllIndicators();
    }

    private void OnTap()
    {
        for (int i = 0; i < pebbleSpawnWithEachTap; i++)
        {
            if (pebbleCount < maxPebbles)
            {
                SpawnPebble();
            }
            else
            {
                return;
            }
        }
    }

    private void SpawnPebble()
    {
        if (!isFocused) return;

        SFXManager.Instance.PlayRainStickParticleSpawnSound();
        GameObject pebble = Instantiate(rainstickPebble, partcleSpawnPoint);
        pebble.transform.localScale = Vector3.one * pebblesScale;
        presentPebbles.Add(pebble);
        pebbleCount++;

        if (pebbleCount >= maxPebbles)
        {
            rotatingHandler.canRotate = true;
            InteractionIndicatorManager.Instance.ShowIndicator(Interaction.Rotate, rotateIndicatorPos,
                                                               showIndicatorAfterSeconds, 3);
        }
    }

    private void DestroyAllPebbles()
    {
        for (int i = 0; i < presentPebbles.Count; i++)
        {
            Destroy(presentPebbles[i]);
        }

        pebbleCount = 0;
        collectivePebblesHitCount.RuntimeValue = 0;
    }

    private void IsInNeonZone()
    {
        if (pebbleCount <= 0)
        {
            isInNeonState = false;
            return;
        }

        int _averageHit = collectivePebblesHitCount.RuntimeValue / pebbleCount;
        if (_averageHit <= maxHitPerPebbleNeonThreshold &&
            _averageHit >= minHitPerPebbleNeonThreshold)
        {
            isInNeonState = true;
            if (!isAddingToNeon)
            {
                StartCoroutine(NeonState());
                StartCoroutine(SparkleEffectLoop());
            }
        }
        else
        {
            if (isInNeonState)
                isInNeonState = false;
        }

        if (_averageHit >= maxHitPerPebbleNeonThreshold)
        {
            rotatingHandler.canRotate = false;
            DestroyAllPebbles();
        }
    }

    IEnumerator NeonState()
    {
        isAddingToNeon = true;

        while (isInNeonState)
        {
            yield return new WaitForSeconds(1);
            CollectionManager.Instance.AddToCollectiveNeonTime(gameObject, amountToAddToCollectiveNeonEachSecond);
        }

        isAddingToNeon = false;
    }
    
    private IEnumerator SparkleEffectLoop()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            if (isInNeonState)
            {
                GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.Positive);
            }
        }
    }
}