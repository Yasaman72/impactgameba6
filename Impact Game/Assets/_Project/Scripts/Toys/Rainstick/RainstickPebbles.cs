﻿using Sirenix.OdinInspector;
using UnityEngine;

public class RainstickPebbles : MonoBehaviour
{
    [SerializeField] private float collisionDetectionInterval;
    [SerializeField] private float secondBeforeReduceOneHitCount = 1;
    [SerializeField] private IntVariable collectivePebblesHitCount;


    [SerializeField, ReadOnly] private int currentHitCount;
    [SerializeField, ReadOnly] private bool isInNeonZone;
    private float lastColliderTime;

    private void OnEnable()
    {
        currentHitCount = 0;
        lastColliderTime = Time.time;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (Time.time - lastColliderTime > collisionDetectionInterval)
        {
            currentHitCount++;
            collectivePebblesHitCount.RuntimeValue++;
        }

        CancelInvoke(nameof(ReduceHitCount));
        Invoke(nameof(ReduceHitCount), secondBeforeReduceOneHitCount);

        lastColliderTime = Time.time;
    }

    private void ReduceHitCount()
    {
        if (currentHitCount - 1 >= 0)
        {
            currentHitCount--;
            collectivePebblesHitCount.RuntimeValue--;
        }

        Invoke(nameof(ReduceHitCount), secondBeforeReduceOneHitCount);
    }


}