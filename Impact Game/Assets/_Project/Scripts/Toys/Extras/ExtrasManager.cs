﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ExtrasManager : GeneralInteractaboleManager
{
    [Header("Item Specific")]
    [SerializeField] private float desaturateAfter = 3;

    [Space]
    [SerializeField] private GameEvent tap;

    [Space]
    [SerializeField] private Animator animator;

    [SerializeField] private GameObject[] objectsToEnableOnTap;
    [SerializeField] private ParticleSystem[] particles;

    [Header("neon state")]
    [SerializeField] private int tapsCountForScore = 3;

    [SerializeField] private int score = 1;

    [Header("Interaction Indicator")]
    [SerializeField] private float showIndicatorAfterSeconds = 1;
    [SerializeField] private float disableIndicatorAfter = 3;
    [SerializeField] private Transform tapIndicatorLocation;

    private int currentTapCount;

    protected override void OnEnable()
    {
        base.OnEnable();
      //  tap.RegisterFunction(OnTap);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
       // tap.RemoveFunction(OnTap);
    }

    public override void FocusItem()
    {
        if (isInFocusMode.RuntimeValue || isFocused) return;
        base.FocusItem();
        currentTapCount = 0;
        InteractionIndicatorManager.Instance.ShowIndicator(Interaction.Tap, tapIndicatorLocation, showIndicatorAfterSeconds, disableIndicatorAfter);
    }

    public override void UnfocusItem()
    {
        base.UnfocusItem();
        InteractionIndicatorManager.Instance.CancelEnabling();
    }


    public override void OnPointerDown(PointerEventData eventData)
    {
        OnTap();
        base.OnPointerDown(eventData);
    }

    private void OnTap()
    {
        if (!isFocused) return;
        if (animator != null)
        {
            animator.SetTrigger("onTap");
        }

        for (int i = 0; i < objectsToEnableOnTap.Length; i++)
        {
            objectsToEnableOnTap[i].SetActive(true);
        }

        for (int i = 0; i < particles.Length; i++)
        {
            particles[i].gameObject.SetActive(true);
            particles[i].Stop();
            particles[i].Play();
        }

        if (!isInNeonState)
        {
            isInNeonState = true;
            CancelInvoke(nameof(ExitNeon));
            Invoke(nameof(ExitNeon), desaturateAfter);
        }

        currentTapCount++;
        if (currentTapCount >= tapsCountForScore)
        {
            currentTapCount = 0;
            CollectionManager.Instance.AddToCollectiveNeonTime(gameObject, score);
            GameVFXManager.instance.SpawnParticleEffect(transform, GameVFXManager.ParticleType.Positive);
        }
        
        SFXManager.Instance.PlayTapSound();
    }

    private void ExitNeon()
    {
        isInNeonState = false;
    }
}