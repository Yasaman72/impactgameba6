﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseInventoryButton : MonoBehaviour
{
    [Space]
    [SerializeField] private BoolVariable isInventoryOpen;
    [SerializeField] private Animator animator;

    private void OnEnable()
    {
        onInventoryStateChanged();
        isInventoryOpen.AddListener(onInventoryStateChanged);
    }

    private void OnDisable()
    {
        isInventoryOpen.RemoveListener(onInventoryStateChanged);
    }
    
    private void onInventoryStateChanged()
    {
        SFXManager.Instance.PlayInventoryOpenCloseSound();
        animator.SetBool("open", isInventoryOpen.RuntimeValue);
    }
}
