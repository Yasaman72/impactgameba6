﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HelpTool : MonoBehaviour
{
    [SerializeField] private DragableFlyback dragableUi;

    private void OnEnable()
    {
        dragableUi.onDragOnSomething += OnDragOnItem;
    }

    private void OnDisable()
    {
        dragableUi.onDragOnSomething -= OnDragOnItem;
    }

    private void OnDragOnItem(GameObject other)
    {
        HelperPanelManager.Instance.ShowHelp(other.GetComponent<GeneralInteractaboleManager>().helpText
                                             , other.GetComponent<GeneralInteractaboleManager>().helpShowTime);
    }
}
