﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class TrashTool : MonoBehaviour
{
    [Space]
    [SerializeField] private DragableFlyback dragableFlyback;
    [Space]
    [SerializeField] private Animator animator;
    [Space]
    [SerializeField, ReadOnly] public GameObject lastSweatEffect;
    [SerializeField, ReadOnly] public GameObject lastHoveredItem;
    
    private void OnEnable()
    {
        dragableFlyback.onDragOnSomething += OnDragOnItem;
        dragableFlyback.onHoveredOnSomething += OnHovering;
    }

    private void OnDisable()
    {
        dragableFlyback.onDragOnSomething -= OnDragOnItem;
        dragableFlyback.onHoveredOnSomething -= OnHovering;
    }

    private void OnHovering(GameObject hoveredItem)
    {
        if (hoveredItem == null)
        {
            if (lastSweatEffect != null) lastSweatEffect.SetActive(false);
            lastHoveredItem = null;
            return;
        }

        if (hoveredItem == lastHoveredItem && lastSweatEffect.activeInHierarchy)
            return;

        if (lastSweatEffect == null)
        {
            lastSweatEffect = GameVFXManager.instance.SpawnParticleEffect(hoveredItem.transform,
                                                                          GameVFXManager.ParticleType.WaterSplash);
        }

     
            lastSweatEffect.transform.position =
                hoveredItem.GetComponent<GeneralInteractaboleManager>().sweatPosition.position;
            lastSweatEffect.transform.rotation =
                hoveredItem.GetComponent<GeneralInteractaboleManager>().sweatPosition.rotation;
            lastSweatEffect.transform.localScale =
                hoveredItem.GetComponent<GeneralInteractaboleManager>().sweatPosition.localScale;
        

        lastSweatEffect.SetActive(true);
        
        lastHoveredItem = hoveredItem;
    }

    private void OnDragOnItem(GameObject other)
    {
        if (lastSweatEffect != null) Destroy(lastSweatEffect);

        GameVFXManager.instance.SpawnParticleEffect(other.transform, GameVFXManager.ParticleType.Smoke);

        other.GetComponent<GeneralInteractaboleManager>().InventoryItem.interactableItem.isOnGround = false;
        other.GetComponent<GeneralInteractaboleManager>().InventoryItem.Enable();
        Destroy(other);

        animator.SetTrigger("hit");
    }
}