﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloseTool : MonoBehaviour
{
    [SerializeField] private BoolVariable isInFocusMode;
    [SerializeField] private GameObject exitFocus;
    [SerializeField] private GameObject closeInventory;
    
    private void OnEnable()
    {
        OnFocusModeChange();
        isInFocusMode.AddListener(OnFocusModeChange); 
    }

    private void OnDisable()
    {
        isInFocusMode.RemoveListener(OnFocusModeChange); 
    }

    private void OnFocusModeChange()
    {
        exitFocus.SetActive(isInFocusMode.RuntimeValue);
        closeInventory.SetActive(!isInFocusMode.RuntimeValue);
    }


}