﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionIndicatorManager : MonoSingleton<InteractionIndicatorManager>
{
    [SerializeField] private interactionData rotateIndicator;
    [SerializeField] private interactionData tapIndicator;
    [SerializeField] private interactionData swipeRightIndicator;
    [SerializeField] private interactionData swipeLeftIndicator;
    [SerializeField] private interactionData swipeUpIndicator;
    [SerializeField] private interactionData swipeDownIndicator;
    [SerializeField] private interactionData swipeDownHoldIndicator;
    [SerializeField] private interactionData swipeUpHoldIndicator;

    private interactionData indicatorToEnable, indicatorToDisable;
    private Dictionary<Interaction, interactionData> indicatorsDictionary = new Dictionary<Interaction, interactionData>();

    [System.Serializable]
    public struct interactionData
    {
        public interactionIndicator indicator;
        public GameEvent interactionEvent;
    }

    private void Start()
    {
        PopulateDictionary();
        DisableAllIndicators();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void PopulateDictionary()
    {
        indicatorsDictionary.Add(Interaction.Rotate, rotateIndicator);
        indicatorsDictionary.Add(Interaction.Tap, tapIndicator);
        indicatorsDictionary.Add(Interaction.SwipeRight, swipeRightIndicator);
        indicatorsDictionary.Add(Interaction.SwipeLeft, swipeLeftIndicator);
        indicatorsDictionary.Add(Interaction.SwipeUp, swipeUpIndicator);
        indicatorsDictionary.Add(Interaction.SwipeDown, swipeDownIndicator);
        indicatorsDictionary.Add(Interaction.SwipeDownHold, swipeDownHoldIndicator);
        indicatorsDictionary.Add(Interaction.SwipeUpHold, swipeUpHoldIndicator);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="interactionType"></param>
    /// <param name="trans"></param>
    /// <param name="waitTime"></param>
    /// <param name="fadeAfter">if it's less than 0, it won't fade after a given time</param>
    public void ShowIndicator(Interaction interactionType, Transform trans, float waitTime = 0, float fadeAfter = 2)
    {
        if (!indicatorsDictionary.TryGetValue(interactionType, out indicatorToEnable)) return;
        indicatorToEnable.indicator.disableAfterTime = fadeAfter;
        Invoke(nameof(RegisterCancel), .5f);
        StopAllCoroutines();
        StartCoroutine(EnableIndicator(indicatorToEnable.indicator.gameObject, trans, waitTime));
    }

    public IEnumerator EnableIndicator(GameObject indicator,Transform trans, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        
        indicator.SetActive(true);
        indicator.transform.position = trans.position;
        indicator.transform.rotation = trans.rotation;
        indicatorToEnable.interactionEvent.RemoveFunction(CancelEnabling);
    }

    private void RegisterCancel()
    {
        indicatorToEnable.interactionEvent.RegisterFunction(CancelEnabling);
    }
    
    // when the interaction is done there is no need to show the indicator
    public void CancelEnabling()
    {
        if(indicatorToEnable.indicator == null) return;
        Debug.Log("canceled enabling " + indicatorToEnable.indicator);
        
        indicatorToEnable.interactionEvent.RemoveFunction(CancelEnabling);
        StopAllCoroutines();
    }

    public void DisableIndicator(Interaction interactionType)
    {
        if (!indicatorsDictionary.TryGetValue(interactionType, out indicatorToDisable)) return;
     
        Debug.Log("disabled " + indicatorToEnable);

        StopAllCoroutines();
        indicatorToDisable.indicator.gameObject.SetActive(false);
    }

    public void DisableAllIndicators()
    {
        Debug.Log("disabling all indicators");
        StopAllCoroutines();
        foreach (var interactable in indicatorsDictionary)
        {
            interactable.Value.indicator.gameObject.SetActive(false);
        }
    }
}

public enum Interaction
{
    Rotate,
    Tap,
    SwipeUp,
    SwipeDown,
    SwipeDownHold,
    SwipeUpHold,
    SwipeLeft,
    SwipeRight
}