﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HandleCollidersOnDrag : MonoBehaviour
{
    [SerializeField] private BoolVariable isDraggingSomething;
    [SerializeField] private GeneralInteractaboleManager interactaboleManager;
    [SerializeField] private bool disableAllChildrenCollider = true;

    [Space(20)]
    [SerializeField] private Collider2D[] colliderOnDragging;
    [SerializeField] private Collider2D[] colliderOnNoDrag;

    private void Start()
    {
        ActivateParentCollider();
    }

    private void OnEnable()
    {
        isDraggingSomething.AddListener(OnDraggingSomething);
        interactaboleManager.onFocusChanged += OnItemFocusChanged;
    }

    private void OnDisable()
    {
        isDraggingSomething.RemoveListener(OnDraggingSomething);
        interactaboleManager.onFocusChanged -= OnItemFocusChanged;
    }

    private void OnItemFocusChanged(bool newFocus)
    {
        if (newFocus)
        {
            ActivateChildrenCollider();
        }
        else
        {
            ActivateParentCollider();
        }
    }
    
    private void OnDraggingSomething()
    {
        if (isDraggingSomething.RuntimeValue)
        {
            ActivateParentCollider();
        }
        else
        {
           // ActivateChildrenCollider();
        }
    }

    private void ActivateParentCollider()
    {
        OnDragColliderActivation(true);
        OnNoDragColliderActivation(false);
    }
    
    private void ActivateChildrenCollider()
    {
        OnDragColliderActivation(false);
        OnNoDragColliderActivation(true);
    }

    private void OnDragColliderActivation(bool activate)
    {
        foreach (Collider2D _collider2D in colliderOnDragging)
        {
            _collider2D.enabled = activate;
        }
    }

    private void OnNoDragColliderActivation(bool activate)
    {
        foreach (var _collider2D in colliderOnNoDrag)
        {
            _collider2D.enabled = activate;
        }

        if (!disableAllChildrenCollider) return;

        var collidersInChildren = transform.GetComponentsInChildren<Collider2D>();

        foreach (Collider2D childCollider in collidersInChildren)
        {
            if (colliderOnDragging.Contains(childCollider)) continue;
            childCollider.enabled = activate;
        }
    }
}