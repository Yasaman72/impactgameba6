﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Audio;

public class MusicManager : MonoBehaviour
{
    [Header("reads")]
    [SerializeField] private IntVariable presentItemsCount;
  [Space]
    [SerializeField] private BoolVariable isMusicOn;
    [SerializeField] private AudioMixer mixer;

    [Space]
    [SerializeField] private float fadingDurationSeconds = 2;
    [Space]
    [SerializeField] private AudioData[] audioData;

    [System.Serializable]
    struct AudioData
    {
        public AudioSource audioSource;
        public int presentItemsCountToActivate;
        public float maxVolume;
    }

    private void OnEnable()
    {
        presentItemsCount.AddListener(OnItemCountChanged);
        OnItemCountChanged();
        MuteAllAudios(false);
        isMusicOn.AddListener(OnMusicChange);
    }

    private void OnDisable()
    {
        presentItemsCount.RemoveListener(OnItemCountChanged);
        isMusicOn.RemoveListener(OnMusicChange);
    }

    private void OnMusicChange()
    {
        if (isMusicOn.RuntimeValue)
        {
            mixer.SetFloat("musicVol", 0);
        }
        else
        {
            mixer.SetFloat("musicVol", -80);
        }
    }
    
    private void OnItemCountChanged()
    {
        foreach (var data in audioData)
        {
            if (data.presentItemsCountToActivate <= presentItemsCount.RuntimeValue)
            {
                data.audioSource.DOFade(data.maxVolume, fadingDurationSeconds);
            }
            else
            {
                data.audioSource.DOFade(0, fadingDurationSeconds);
            }
        }
    }

    private void MuteAllAudios(bool fade)
    {
        foreach (var data in audioData)
        {
            if (fade)
            {
                data.audioSource.DOFade(0, fadingDurationSeconds);
            }
            else
            {
                data.audioSource.volume = 0;
            }
        }
    }
}