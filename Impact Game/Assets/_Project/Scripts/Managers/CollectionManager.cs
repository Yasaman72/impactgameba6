﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionManager : MonoSingleton<CollectionManager>
{
    [SerializeField] private bool debug;
    [SerializeField] private IntVariable collectiveScore;

    public void AddToCollectiveNeonTime(GameObject effectingObject, int valueToAdd)
    {
        collectiveScore.RuntimeValue += valueToAdd;

        if (debug)
        {
            Debug.Log("<color=yellow>" + effectingObject.name + " added </color>" + valueToAdd +
                      "<color=yellow> to the collective neon time. current amount: </color>" +
                      collectiveScore.RuntimeValue);
        }
    }
}