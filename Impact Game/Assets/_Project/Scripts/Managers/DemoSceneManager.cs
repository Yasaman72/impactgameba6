﻿using Unity.Mathematics;
using UnityEngine;

public class DemoSceneManager : MonoBehaviour
{
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private InteractableItem[] interactableItems;

    private GameObject currentObject;

    public void ShowItem(int index)
    {
        if (currentObject != null)
            Destroy(currentObject);

        currentObject = Instantiate(interactableItems[index].toyObject, spawnPoint.transform.position, quaternion.identity, spawnPoint);
        currentObject.transform.localScale = Vector3.one * interactableItems[index].placedScale;
    }
}