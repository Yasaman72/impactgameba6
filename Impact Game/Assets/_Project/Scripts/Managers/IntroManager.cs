﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class IntroManager : MonoBehaviour
{
    [SerializeField] private BoolVariable isFirstTimePlaying;
    [Space]
    [SerializeField] private GameEvent CollectCollectable;
    [SerializeField] private BoolVariable isDraggingSomething;
    [SerializeField] private CanvasGroup UiCanvasGroup;
    [SerializeField] private Animator backgroundAnimator;
    [SerializeField] private Animator introAnimator;
    [SerializeField] private WaveManager waveManager;
    [SerializeField] private GameObject[] objectsToEnableInIntro;
    [Header("star help")]
    [SerializeField] private float starHelpWaitTime;
    [SerializeField] private string starHelpText;
    [Header("drag help")]
    [SerializeField] private float dragHelpWaitTime;
    [SerializeField] private string dragHelpText;

    private void Start()
    {
        if (!isFirstTimePlaying.RuntimeValue)
        {
            OnNotFirstPlayTime();
            return;
        }

        IntroVisualSetup();
        
        CollectCollectable.RegisterFunction(OnStarClick);
        StartCoroutine(ShowHelp(starHelpText, starHelpWaitTime));
        waveManager.waveCanMove = false;
    }

    private void IntroVisualSetup()
    {
        for (int i = 0; i < objectsToEnableInIntro.Length; i++)
        {
            objectsToEnableInIntro[i].SetActive(true);
        }

        UiCanvasGroup.alpha = 0;
        backgroundAnimator.SetBool("Intro", true);
    }

    private void OnNotFirstPlayTime()
    {
        for (int i = 0; i < objectsToEnableInIntro.Length; i++)
        {
            objectsToEnableInIntro[i].SetActive(false);
        }
        
        UiCanvasGroup.alpha = 1;
        backgroundAnimator.enabled = false;
    }

    private void OnStarClick()
    {
        TipManager.Instance.DisableTip();
        CollectCollectable.RemoveFunction(OnStarClick);
        UiCanvasGroup.DOFade(1, 3);
        
        // show drag help
        StopAllCoroutines();
        isDraggingSomething.AddListener(OnFirstDrag);
        StartCoroutine(ShowHelp(dragHelpText, dragHelpWaitTime));
        
        backgroundAnimator.SetTrigger("move");
        introAnimator.SetTrigger("drawAway");
        waveManager.waveCanMove = true;
        isFirstTimePlaying.SetFalse();
    }
    
    private IEnumerator ShowHelp(string helpText, float waitTime)
    {
        yield return  new WaitForSeconds(waitTime);
        TipManager.Instance.ShowTip(helpText);
    }

    private void OnFirstDrag()
    {
        StopAllCoroutines();
        TipManager.Instance.DisableTip();
        isDraggingSomething.RemoveListener(OnFirstDrag);
    }
    
}