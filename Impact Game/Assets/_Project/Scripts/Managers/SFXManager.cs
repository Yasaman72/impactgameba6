﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

public class SFXManager : MonoSingleton<SFXManager>
{
    
    [Header("Sound Effects")] [SerializeField]
    private AudioClip poofAudioClip;
    [Space]
    [SerializeField] private BoolVariable isSfxOn;
    [SerializeField] private AudioMixer mixer;

    [Space]
    [SerializeField] private AudioClip itemDestroyAudioClip;
    [SerializeField] private AudioClip itemPickUpAudioClip;
    [SerializeField] private AudioClip inventoryOpenCloseAudioClip;
    [SerializeField] private AudioClip hammerPickUpAudioClip;
    [SerializeField] private AudioClip paperFoldingAudioClip;
    [SerializeField] private AudioClip coinFlipAudioClip;
    [SerializeField] private AudioClip coinSpinAudioClip;
    [SerializeField] private AudioClip bulbPluckAudioClip;
    [SerializeField] private AudioClip waveAudioClip;
    [SerializeField] private AudioClip rainStickRotateAudioClip;
    [SerializeField] private AudioClip rainStickParticleSpawnAudioClip;
    [SerializeField] private AudioClip tapAudioClip;
    [SerializeField] private AudioClip pumpDownAudioClip;
    [SerializeField] private AudioClip collectAudioClip;

    [Header("Audio Sources")] 
    [SerializeField] private AudioSource firstPublicAudioSource;
    [SerializeField] private AudioSource secondPublicAudioSource;
    [SerializeField] private AudioSource paperAudioSource;
    [SerializeField] private AudioSource coinAudioSource;
    [SerializeField] private AudioSource bulbAudioSource;
    [SerializeField] private AudioSource waveAudioSource;
    [SerializeField] private AudioSource rainStickAudioSource;
    [SerializeField] private AudioSource pumpAudioSource;

    public bool isPlayingCoinSound = false;
    public bool isPlayingRainStickSpinSound = false;

    private void OnEnable()
    {
        isSfxOn.AddListener(OnSFXChanged);
    }

    private void OnDisable()
    {
        isSfxOn.RemoveListener(OnSFXChanged);
    }

    private void OnSFXChanged()
    {
        if (isSfxOn.RuntimeValue)
        {
            mixer.SetFloat("SFXVol", 0);
        }
        else
        {
            mixer.SetFloat("SFXVol", -80);
        }
    }


    public void PlayPoofSound()
    {
        firstPublicAudioSource.pitch = Random.Range(0.8f, 1.2f);
        firstPublicAudioSource.PlayOneShot(poofAudioClip);
    }

    public void PlayDestroySound()
    {
        if(secondPublicAudioSource == null) return;
        secondPublicAudioSource.pitch = Random.Range(0.8f, 1.2f);
        secondPublicAudioSource.PlayOneShot(itemDestroyAudioClip);
    }

    public void PlayHammerPickUpSound()
    {
        secondPublicAudioSource.pitch = Random.Range(0.8f, 1.2f);
        secondPublicAudioSource.PlayOneShot(hammerPickUpAudioClip);
    }

    public void PlayItemPickUpSound()
    {
        secondPublicAudioSource.pitch = Random.Range(0.8f, 1.2f);
        secondPublicAudioSource.PlayOneShot(itemPickUpAudioClip);
    }
    
    public void PlayInventoryOpenCloseSound()
    {
        secondPublicAudioSource.pitch = Random.Range(0.8f, 1.2f);
        secondPublicAudioSource.PlayOneShot(inventoryOpenCloseAudioClip);
    }

    public void PlayPaperFoldingSound()
    {
        paperAudioSource.pitch = Random.Range(0.8f, 1.2f);
        paperAudioSource.PlayOneShot(paperFoldingAudioClip);
    }

    public void PlayCoinFlipSound()
    {
        coinAudioSource.Stop();
        coinAudioSource.loop = false;
        isPlayingCoinSound = false;
        coinAudioSource.pitch = Random.Range(0.8f, 1.2f);
        coinAudioSource.PlayOneShot(coinFlipAudioClip);
    }

    public void PlayCoinSpinSound()
    {
        isPlayingCoinSound = true;
        coinAudioSource.pitch = Random.Range(0.8f, 1.2f);
        coinAudioSource.clip = coinSpinAudioClip;
        coinAudioSource.loop = true;
        coinAudioSource.Play();
    }

    public void StopCoinSpinSound()
    {
        isPlayingCoinSound = false;
        coinAudioSource.Stop();
        coinAudioSource.loop = false;
    }
    
    public void PlayPetalPluckSound()
    {
        bulbAudioSource.pitch = Random.Range(0.8f, 1.2f);
        bulbAudioSource.PlayOneShot(bulbPluckAudioClip);
    }
    
    public void PlayWaveSound()
    {
        waveAudioSource.pitch = Random.Range(0.8f, 1.2f);
        waveAudioSource.PlayOneShot(waveAudioClip);
    }
    
    public void PlayRainStickSpinSound()
    {
        isPlayingRainStickSpinSound = true;
        rainStickAudioSource.pitch = Random.Range(0.8f, 1.2f);
        rainStickAudioSource.clip = rainStickRotateAudioClip;
        rainStickAudioSource.loop = true;
        rainStickAudioSource.Play();
    }
    
    public void StopRainStickSpinSound()
    {
        isPlayingRainStickSpinSound = false;
        rainStickAudioSource.Stop();
        rainStickAudioSource.loop = false;
    }
    
    public void PlayRainStickParticleSpawnSound()
    {
        rainStickAudioSource.Stop();
        rainStickAudioSource.loop = false;
        isPlayingRainStickSpinSound = false;
        rainStickAudioSource.pitch = Random.Range(0.8f, 1.2f);
        rainStickAudioSource.PlayOneShot(rainStickParticleSpawnAudioClip);
    }
    
    public void PlayTapSound()
    {
        secondPublicAudioSource.pitch = Random.Range(0.8f, 1.2f);
        secondPublicAudioSource.PlayOneShot(tapAudioClip);
    }
    
    public void PlayPumpDownSound()
    {
        pumpAudioSource.pitch = Random.Range(0.8f, 1.2f);
        pumpAudioSource.PlayOneShot(pumpDownAudioClip);
    }
    
    public void PlayCollectSound()
    {
        secondPublicAudioSource.pitch = Random.Range(0.8f, 1.2f);
        secondPublicAudioSource.PlayOneShot(collectAudioClip);
    }
}