﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[ExecuteInEditMode]
public class ResetGame : MonoBehaviour
{
    [SerializeField] private bool resetOnStart;
    [SerializeField] private InteractableItemsManager interactableItemsManager;

    private void Awake()
    {
        if (resetOnStart)
            Reset();
    }

    [Button]
    public void Reset()
    {
        PlayerPrefs.DeleteAll();
        interactableItemsManager.RemoveAllItems();

        Debug.Log("<color=#DB27D2> Reset Game DATA! </color>");
    }
}