﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundManager : MonoBehaviour
{
    [Header("reads")]
    [SerializeField] private IntVariable presentItemsCount;

    [SerializeField] private string enterParameter = "Enter";
    [SerializeField] private string speedParameter = "Speed";


    [Space]
    [SerializeField] private BackgroundData[] backgroundData;

    [System.Serializable]
    struct BackgroundData
    {
        public Animator backGroundAnimator;
        public int presentItemsCountToActivate;
        public float animationSpeed;
    }

    private void OnEnable()
    {
        presentItemsCount.AddListener(OnItemCountChanged);
        OnItemCountChanged();
    }

    private void OnDisable()
    {
        presentItemsCount.RemoveListener(OnItemCountChanged);
    }

    private void OnItemCountChanged()
    {
        foreach (var data in backgroundData)
        {
            if (data.presentItemsCountToActivate <= presentItemsCount.RuntimeValue)
            {
                data.backGroundAnimator.SetFloat(speedParameter, data.animationSpeed);
                data.backGroundAnimator.SetBool(enterParameter, true);
            }
            else
            {
                data.backGroundAnimator.SetFloat(speedParameter, data.animationSpeed);
                data.backGroundAnimator.SetBool(enterParameter, false);
            }
        }
    }
}