﻿using UnityEngine;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour
{
    [SerializeField] public InteractableItem interactableItem;
    [SerializeField] private CheckTouchingObjects checkTouchingObjects;

    [Space]
    [SerializeField] private BoolVariable isDraggingSomething;

    [SerializeField] private DragableUi dragableUi;
    [SerializeField] private Image image;
    [SerializeField] private GameObject emptySlot;
    [SerializeField] private GameObject imageSlot;
    [SerializeField] private GameObject newIndicator;

    private Vector3 initialPosition;


    private void Awake()
    {
        image.material = new Material(image.material);
        image.material.SetFloat("_Saturation", 1);

        initialPosition = transform.position;
    }

    public void SetInventoryItem(InteractableItem item)
    {
        interactableItem = item;
        if (interactableItem != null) interactableItem.isEnableInInventory = true;
    }

    public void SetupUi()
    {
        if (interactableItem == null)
        {
            emptySlot.SetActive(true);
            imageSlot.SetActive(false);
            newIndicator.SetActive(false);
        }
        else
        {
            newIndicator.SetActive(interactableItem.isNew);
            emptySlot.SetActive(false);
            imageSlot.SetActive(true);

            image.sprite = interactableItem.itemInventoryIcon;
        }

        dragableUi.onDragEnd += OnDragEnd;
        dragableUi.onDragStart += OnDragStart;

        if (interactableItem != null && interactableItem.isEnableInInventory)
        {
            if (interactableItem.isOnGround)
                Disable();
            else
                Enable();
        }
        else
        {
            Disable();
        }
    }

    private void OnDragEnd()
    {
        if (checkTouchingObjects.CheckTouchWithLayer(initialPosition, interactableItem))
        {
            interactableItem.isOnGround = true;
            Disable();
        }

        isDraggingSomething.SetFalse();
    }

    private void OnDragStart()
    {
        isDraggingSomething.SetTrue();
    }

    public void Disable()
    {
        if (interactableItem != null && interactableItem.isNew)
        {
            interactableItem.isNew = false;
            newIndicator.SetActive(false);
        }

        image.material.SetFloat("_Saturation", 0);
        if (interactableItem != null) interactableItem.isEnableInInventory = false;
        dragableUi.canDrag = false;
    }

    public void Enable()
    {
        if (interactableItem != null)
        {
            if (interactableItem.isOnGround)
            {
                return;
            }

            interactableItem.isEnableInInventory = true;
            interactableItem.isOnGround = false;
        }

        image.material.SetFloat("_Saturation", 1);
        dragableUi.canDrag = true;
    }
}