﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundElement : MonoBehaviour
{
    [SerializeField] private Animator animator;

    public void Move(float newValue)
    {
        StopAllCoroutines();
        StartCoroutine(LerpToPosition(newValue));
    }
    
   IEnumerator LerpToPosition(float newValue)
    {
        float t = 0.0f;
        float initialValue = animator.GetFloat("progression");

        while (t < 1)
        {
            float lerpValue = Mathf.Lerp(initialValue, newValue, t);
            animator.SetFloat("progression", lerpValue);
            t += 3f * Time.deltaTime;
            yield return new WaitForSeconds(0.1f);
        }

        animator.SetFloat("progression", newValue);
    }
}
