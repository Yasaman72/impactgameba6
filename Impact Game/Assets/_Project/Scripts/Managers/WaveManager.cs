﻿using System.Collections;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using Random = UnityEngine.Random;

public class WaveManager : MonoBehaviour
{
    [ReadOnly] public bool waveCanMove = true;
    [SerializeField] private float waveIntervalSecond = 20;
    [SerializeField] private IntVariable collectiveScore;

    [Space]
    [SerializeField] private InteractableItemsManager interactableItemsManager;

    [HideInInspector] private bool gainInteractablesRandomly = false;
    [SerializeField] private InteractableItem[] interactableItems;
    [SerializeField] private GameEvent formWave;
    [SerializeField] private Animator animator;
    [SerializeField] private string waveFormParameter = "FormWave";

    [Space]
    [SerializeField] private GameObject collectableItem;

    [SerializeField] private Transform[] collectableSlots;

    [Space]
    [SerializeField] private GameEvent collectCollectable;

    private int visibleCollectablesCount;
    
    private void OnEnable()
    {
        StartCoroutine(Wave());
        collectCollectable.RegisterFunction(OnCollectItem);

        interactableItems.Sort(delegate(InteractableItem a, InteractableItem b) {
            return (a.minRequiredCollectiveNeonStateTime).CompareTo(b.minRequiredCollectiveNeonStateTime);
        });
    }

    private void OnDisable()
    {
        collectCollectable.RemoveFunction(OnCollectItem);
    }

    private IEnumerator Wave()
    {
        while (true)
        {
            yield return new WaitForSeconds(waveIntervalSecond);
            if (!waveCanMove) continue;
            formWave.Raise();
            animator.SetTrigger(waveFormParameter);
            SFXManager.Instance.PlayWaveSound();
        }
    }

    // is called in the wave animation
    public void AddCollectables()
    {
        if (HasCollectedAllItems()) return;
        if (visibleCollectablesCount > 0) return;
        if (visibleCollectablesCount >= collectableSlots.Length) return;
        if (visibleCollectablesCount >=
            interactableItems.Length - interactableItemsManager.CollectedItemsCount()) return;

        // int collectableCount = Random.Range(1, 2);
        // for (int i = 0; i < collectableCount; i++)
        // {
        if (!CanSpawnTheNextCollectable()) return;

        Instantiate(collectableItem, collectableSlots[GetCollectableParentindex()]);
        // }

        // visibleCollectablesCount += collectableCount;
        visibleCollectablesCount++;
    }

    private int GetCollectableParentindex()
    {
        int slotIndex = Random.Range(0, collectableSlots.Length);
        if (collectableSlots[slotIndex].childCount > 0)
            return GetCollectableParentindex();

        return slotIndex;
    }

    private bool CanSpawnTheNextCollectable()
    {
       var newxtItemRequiredScore = interactableItems[interactableItemsManager.CollectedItemsCount()]
            .minRequiredCollectiveNeonStateTime;
        return collectiveScore.RuntimeValue >= newxtItemRequiredScore;
    }

    private void OnCollectItem()
    {
        InteractableItem item = null;
        if (gainInteractablesRandomly)
        {
            item = GetRandomNewInteractable();
        }
        else
        {
            item = interactableItems[interactableItemsManager.CollectedItemsCount()];
        }

        interactableItemsManager.AddToCollectedItems(item);
        visibleCollectablesCount--;
    }

    private InteractableItem GetRandomNewInteractable()
    {
        InteractableItem newItem = interactableItems[Random.Range(0, interactableItems.Length)];

        if (interactableItemsManager.ContainTheItem(newItem))
            return GetRandomNewInteractable();

        return newItem;
    }

    public bool HasCollectedAllItems()
    {
        foreach (var item in interactableItems)
        {
            if (!interactableItemsManager.ContainTheItem(item)) return false;
        }

        return true;
    }
}