﻿using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class FocusManager : MonoSingleton<FocusManager>
{
    [Header("writes")]
    [SerializeField] private BoolVariable isInFocusMode;
    [SerializeField] private IntVariable presentItemsCount;

    [Header("listens")]
    [SerializeField] private GameEvent exitFocusMode;

    [Space]
    [ReadOnly] public GeneralInteractaboleManager currentFocusItem;

    [ReadOnly] public List<GeneralInteractaboleManager> allPresentItems
        = new List<GeneralInteractaboleManager>();

    private void OnEnable()
    {
        exitFocusMode.RegisterFunction(ExitFocusMode);
    }

    private void OnDisable()
    {
        exitFocusMode.RemoveFunction(ExitFocusMode);
    }

    public void AddNewItemToGround(GeneralInteractaboleManager item)
    {
        presentItemsCount.RuntimeValue++;
        allPresentItems.Add(item);
        ExitFocusMode();
    }

    public void RemoveItemFromGround(GeneralInteractaboleManager item)
    {
        if (allPresentItems.Contains(item))
        {
            presentItemsCount.RuntimeValue--;
            allPresentItems.Remove(item);
        }
    }

    public void FocusItem(GeneralInteractaboleManager item)
    {
        if (currentFocusItem != null) return;

        item.isFocused = true;
        currentFocusItem = item;
        isInFocusMode.SetTrue();

        ChangedAllItemsVisual(false);
    }

    public void ExitFocusMode()
    {
        if (currentFocusItem == null)
        {
            Debug.Log("no focus item!");
            return;
        }

        isInFocusMode.SetFalse();

        ChangedAllItemsVisual(true);
    }

    private void ChangedAllItemsVisual(bool alsoChangeFocusedItem)
    {
        foreach (var it in allPresentItems)
        {
            if (!alsoChangeFocusedItem && it == currentFocusItem) continue;

            it.UnfocusItem();
        }
        
        if(alsoChangeFocusedItem)
            currentFocusItem = null;
    }
}