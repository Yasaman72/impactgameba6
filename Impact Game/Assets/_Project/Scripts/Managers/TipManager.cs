﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TipManager : MonoSingleton<TipManager>
{
    [SerializeField] private GameObject tipPanel;
    [SerializeField] private TextMeshProUGUI tipText;

    public void ShowTip(string text, float fadeAfter = -1, float showAfter = 0)
    {
        StartCoroutine(Show(text, fadeAfter, showAfter));
    }

    IEnumerator Show(string text, float fadeAfter = -1, float showAfter = 0)
    {
        yield return new WaitForSeconds(showAfter);
        
        tipText.text = text;
        tipPanel.SetActive(true);
        if (fadeAfter >= 0)
        {
            CancelInvoke(nameof(DisableTip));
            Invoke(nameof(DisableTip), fadeAfter);
        }
    }

    public void DisableTip()
    {
        StopAllCoroutines();
        tipPanel.SetActive(false);
    }
}