﻿using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;

public class GeneralInteractaboleManager : MonoBehaviour, IPointerDownHandler
{
    [TextArea(2, 5), HideInInspector] public string helpText;

    [HideInInspector] public float helpShowTime;

    // [ReadOnly] public SlotManager currentSlot;
    [ReadOnly] public InventoryItem InventoryItem;
    public Transform sweatPosition;
    [SerializeField] public GameObject threeLinesEffect;
    [ShowInInspector, ReadOnly] private bool _isFocused;
    [ShowInInspector, ReadOnly] private bool _isInNeonState;

    protected bool isInNeonState
    {
        get => _isInNeonState;
        set
        {
            bool oldValue = _isInNeonState;
            _isInNeonState = value;

            if (_isInNeonState != oldValue)
            {
                ChangeSaturation(saturationChangeTime);
            }
        }
    }

    public bool isFocused
    {
        get => _isFocused;
        set
        {
            _isFocused = value;
            onFocusChanged?.Invoke(_isFocused);
        }
    }

    [SerializeField] protected BoolVariable isInFocusMode;
    [SerializeField] protected float onFocusAlphaValue = 1, onOutFocusAlphaValue = 0.5f;
    [SerializeField] protected float highestSaturation = 1.3f, lowestSaturation = 0.3f;
    [SerializeField] protected float saturationChangeTime = 1;

    [Space]
    [SerializeField]
    protected List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();

    public delegate void OnFocusChanged(bool newValue);

    public event OnFocusChanged onFocusChanged;

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        FocusItem();
    }

    public virtual void FocusItem()
    {
        if (isInFocusMode.RuntimeValue || isFocused) return;
        FocusManager.Instance.FocusItem(this);
        ChangeVisual();
    }

    public virtual void UnfocusItem()
    {
        isFocused = false;
        ChangeVisual();
        InteractionIndicatorManager.Instance.DisableAllIndicators();
    }

    protected void Start()
    {
        foreach (var _spriteRenderer in spriteRenderers)
        {
            _spriteRenderer.material = new Material(_spriteRenderer.material);
            _spriteRenderer.material.SetFloat("_Saturation", 1);
        }

        ChangeSaturation();
    }

    protected virtual void OnEnable()
    {
        //isInFocusMode.AddListener(ChangeVisual);
        FocusManager.Instance.AddNewItemToGround(this);

        if (threeLinesEffect != null) threeLinesEffect.SetActive(true);
        SFXManager.Instance.PlayPoofSound();
    }

    protected virtual void OnDisable()
    {
        if (SFXManager.Instance != null)
            SFXManager.Instance.PlayDestroySound();

       // isInFocusMode.RemoveListener(ChangeVisual);

        if (FocusManager.Instance == null) return;
        FocusManager.Instance.RemoveItemFromGround(this);

        if (FocusManager.Instance.currentFocusItem == this)
        {
            FocusManager.Instance.ExitFocusMode();
        }
    }

    [SerializeField,ReadOnly] private int OriginalOrderInLayer;
    protected void ChangeVisual()
    {
        if (!isActiveAndEnabled) return;

        if (isFocused)
        {
            OriginalOrderInLayer = gameObject.GetComponent<SortingGroup>().sortingOrder;
            gameObject.GetComponent<SortingGroup>().sortingOrder = 22;
        }
        else
        {
            gameObject.GetComponent<SortingGroup>().sortingOrder = OriginalOrderInLayer;
        }

        // float newAlphaValue = 1;
        // if (isInFocusMode.RuntimeValue)
        // {
        //     newAlphaValue = isFocused ? onFocusAlphaValue : onOutFocusAlphaValue;
        // }
        //
        // foreach (var spriteRenderer in spriteRenderers)
        // {
        //     spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b,
        //                                      newAlphaValue);
        // }
    }

    protected void ChangeSaturation(float time = 0)
    {
        float newSaturationValue = isInNeonState ? highestSaturation : lowestSaturation;

        foreach (var spriteRenderer in spriteRenderers)
        {
            spriteRenderer.material.DOFloat(newSaturationValue, "_Saturation", time);
        }
    }
}