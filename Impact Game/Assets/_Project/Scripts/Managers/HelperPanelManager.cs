﻿using TMPro;
using UnityEngine;

public class HelperPanelManager : MonoSingleton<HelperPanelManager>
{
    [SerializeField] private bool setToUpper;
    [SerializeField] private GameObject helpPanel;
    [SerializeField] private TextMeshProUGUI helpText;


    private void OnEnable()
    {
        helpPanel.SetActive(false);
    }

    public void ShowHelp(string text, float disableAfter)
    {
        helpPanel.SetActive(true);
        
        helpText.text = setToUpper ? text.ToUpper() : text;

        CancelInvoke();
        Invoke(nameof(DisableHelp), disableAfter);
    }

    private void DisableHelp()
    {
        helpPanel.SetActive(false);
    }
}