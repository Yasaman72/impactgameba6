﻿using Sirenix.OdinInspector;
using UnityEngine;

public class BackgroundAppearWithSparkles : MonoSingleton<BackgroundAppearWithSparkles>
{
    [Header("reads")]
    [SerializeField] private IntVariable collectiveScore;

    [SerializeField] private int maxCollectableScore = 100;
    [SerializeField] private string enterParameter = "Enter";
    [SerializeField] private string speedParameter = "Speed";

    [Space]
    [SerializeField] private BackgroundData[] backgroundData;

    [ReadOnly, SerializeField] private float progressionPerc;
    [ReadOnly, SerializeField] private float toShowPerc;
    [ReadOnly, SerializeField] private int elementToShowIndex;
    [ReadOnly, SerializeField] private float lastItemProgressionPerc;

    [ReadOnly] public Transform currentBackgroundTransform;

    [System.Serializable]
    struct BackgroundData
    {
        public Animator backGroundAnimator;
        public BackgroundElement backgroundElement;
    }

    private void OnEnable()
    {
        collectiveScore.AddListener(OnItemCountChanged);
        CalculateProgressionData();
        InitialProgression();
    }

    private void OnDisable()
    {
        collectiveScore.RemoveListener(OnItemCountChanged);
    }

    private void OnItemCountChanged()
    {
        CalculateProgressionData();

        if (elementToShowIndex >= backgroundData.Length) return;

        backgroundData[elementToShowIndex].backgroundElement.Move(lastItemProgressionPerc / 100);
        // StartCoroutine(LerpToPosition());
    }

    private void CalculateProgressionData()
    {
        progressionPerc = ((float) collectiveScore.RuntimeValue / maxCollectableScore) * 100;

        toShowPerc = (progressionPerc * backgroundData.Length);
        elementToShowIndex = (int) (toShowPerc / 100);
        if (elementToShowIndex < backgroundData.Length)
            currentBackgroundTransform = backgroundData[elementToShowIndex].backgroundElement.transform;
        lastItemProgressionPerc = Mathf.Abs(((int) (toShowPerc / 100) * 100) - toShowPerc);
    }

    private void InitialProgression()
    {
        if (elementToShowIndex >= backgroundData.Length)
        {
            for (int i = 0; i < backgroundData.Length; i++)
            {
                backgroundData[i].backGroundAnimator.SetFloat("progression", 1);
            }

            return;
        }

        for (int i = 0; i < elementToShowIndex; i++)
        {
            backgroundData[i].backGroundAnimator.SetFloat("progression", 1);
        }

        backgroundData[elementToShowIndex].backGroundAnimator.SetFloat("progression", lastItemProgressionPerc / 100);
    }
}