﻿using TMPro;
using UnityEngine;

public class GuideManager : MonoBehaviour
{
    [SerializeField] private Animator guideAnimator;
    [SerializeField] private BoolVariable isMusicOn, isSoundEffectOn;
    [SerializeField] private TextMeshProUGUI musicText, soundEffectText;
    [SerializeField] private string onText, offText;

    private void Start()
    {
        isMusicOn.AddListener(OnMusicToggle);
        isSoundEffectOn.AddListener(OnSoundEffectToggle);
    }

    private void OnDestroy()
    {
        isMusicOn.RemoveListener(OnMusicToggle);
        isSoundEffectOn.RemoveListener(OnSoundEffectToggle);
    }

    public void TogglePage()
    {
        guideAnimator.SetBool("toggle",!guideAnimator.GetBool("toggle"));
    }

    private void OnMusicToggle()
    {
        musicText.text = isMusicOn.RuntimeValue ? onText : offText;
    }

    private void OnSoundEffectToggle()
    {
        soundEffectText.text = isSoundEffectOn.RuntimeValue ? onText : offText;
    }
}
