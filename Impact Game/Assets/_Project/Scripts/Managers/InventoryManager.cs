﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    [SerializeField] private BoolVariable isInventoryOpen;
    [SerializeField] private Animator inventoryAnimator;
    
    [Space]
    [SerializeField] private GameEvent addedNewItemToInventory;

    [Space]
    [SerializeField] private int collectablesCount;

    [SerializeField] private int onePageSlotCount = 6;
    [SerializeField] private GameObject nextButton, previousButton;
    [SerializeField] private InteractableItemsManager interactableItems;
    [SerializeField] private InventoryItem[] inventoryItems;

    public int itemsInThisPage;
    public int collectedsInThisPage;
    public int currentInventoryPage = 1;
    public int pagesCount;

    private void OnEnable()
    {
        OnInventoryOpenStateChanged();
        isInventoryOpen.AddListener(OnInventoryOpenStateChanged);
        
        addedNewItemToInventory.RegisterFunction(ShowPageContents);

        pagesCount = Mathf.CeilToInt((float) collectablesCount / onePageSlotCount);

        currentInventoryPage = 1;
        ShowPageContents();
        CheckArrowsState();
    }

    private void OnDisable()
    {
        isInventoryOpen.RemoveListener(OnInventoryOpenStateChanged);

        addedNewItemToInventory.RemoveFunction(ShowPageContents);
    }

    private void OnInventoryOpenStateChanged()
    {
      inventoryAnimator.SetBool("open", isInventoryOpen.RuntimeValue);
    }

    public void GoToNextPage()
    {
        currentInventoryPage++;
        ShowPageContents();
        CheckArrowsState();
    }

    public void GoToPreviousPage()
    {
        currentInventoryPage--;
        ShowPageContents();
        CheckArrowsState();
    }

    private void CheckArrowsState()
    {
        nextButton.SetActive(currentInventoryPage != pagesCount);
        previousButton.SetActive(currentInventoryPage != 1);
    }

    private void ShowPageContents()
    {
        itemsInThisPage = 6;
        collectedsInThisPage = interactableItems.collectedItems.Count / currentInventoryPage;
        if (currentInventoryPage == (collectablesCount / onePageSlotCount) + 1)
        {
            itemsInThisPage = collectablesCount % onePageSlotCount;
            if (interactableItems.collectedItems.Count - ((currentInventoryPage-1) * onePageSlotCount) > 0)
            {
                collectedsInThisPage = interactableItems.collectedItems.Count % onePageSlotCount;
            }
            else
            {
                collectedsInThisPage = 0;
            }
        }

        for (int i = 0; i < inventoryItems.Length; i++)
        {
            if (i >= itemsInThisPage)
            {
                inventoryItems[i].gameObject.SetActive(false);
                continue;
            }

            if (i < collectedsInThisPage)
            {
                inventoryItems[i]
                    .SetInventoryItem(interactableItems.collectedItems
                                          [i + ((currentInventoryPage - 1) * onePageSlotCount)]);
            }
            else
            {
                inventoryItems[i].SetInventoryItem(null);
            }

            inventoryItems[i].SetupUi();
            inventoryItems[i].gameObject.SetActive(true);
        }
    }
}