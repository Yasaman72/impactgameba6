﻿using System;
using DigitalRubyShared;
using UnityEngine;

public class SwipeDetection : MonoBehaviour
{
    [SerializeField] private bool continousGestureRecognizer = true;
    [SerializeField] private GameEvent swipeUp, swipeLeft, swipeDown, swipeRight, tap;

    [Space]
    [SerializeField] private GameEvent swipeUpHold;

    [SerializeField] private GameEvent swipeDownHold;
    private SwipeGestureRecognizer swipeGesture;
    private TapGestureRecognizer tapGesture;

    private void Start()
    {
        CreateSwipeGesture();
    }

    private void CreateSwipeGesture()
    {
        // Swipe
        swipeGesture = new SwipeGestureRecognizer();
        swipeGesture.Direction = SwipeGestureRecognizerDirection.Any;
        if (continousGestureRecognizer)
            swipeGesture.EndMode = SwipeGestureRecognizerEndMode.EndContinusously;
        
        swipeGesture.StateUpdated += SwipeGestureCallback;
        swipeGesture.DirectionThreshold = 1.0f; // allow a swipe, regardless of slope
        FingersScript.Instance.AddGesture(swipeGesture);

        // Tap
        tapGesture = new TapGestureRecognizer();
        tapGesture.StateUpdated += TapGestureCallback;
        FingersScript.Instance.AddGesture(tapGesture);
    }

    private void TapGestureCallback(GestureRecognizer gesture)
    {
        if (gesture.State == GestureRecognizerState.Ended)
            tap.Raise();
    }

    private void SwipeGestureCallback(GestureRecognizer gesture)
    {
        float _angle;
        if (gesture.State == GestureRecognizerState.Ended)
        {
            _angle = Vector2.SignedAngle(Vector2.right,
                                         new Vector2(gesture.DeltaX, gesture.DeltaY));
            if (_angle >= 45 && _angle < 135)
            {
                swipeUp.Raise();
            }
            else if (_angle >= 135 || _angle < -135)
            {
                swipeLeft.Raise();
            }
            else if (_angle >= -135 && _angle < -45)
            {
                swipeDown.Raise();
            }
            else
            {
                swipeRight.Raise();
            }

            // DebugText("Swiped from {0},{1} to {2},{3}; velocity: {4}, {5}",
            //           gesture.StartFocusX, gesture.StartFocusY, gesture.FocusX, gesture.FocusY, swipeGesture.VelocityX, swipeGesture.VelocityY);
        }
    }

    private void DebugText(string text, params object[] format)
    {
        //bottomLabel.text = string.Format(text, format);
        Debug.Log(string.Format(text, format));
    }
}